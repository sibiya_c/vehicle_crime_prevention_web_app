import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
    public static void main(String[] args) {
        try {
            checkPassword("Venus~18");
            System.out.println("Valid");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void checkPassword(String newPassword) throws Exception {
        if (newPassword != null) {

            if (newPassword.length() < 8 || newPassword.length() < 8)
                throw new Exception("Password must be 8 characters long.");

            String regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#^_=+/<>;:|.,`~])[A-Za-z\\d$@$!%*?&#^_=+/<>;:|.,`~]{8,}";
            // Create a Pattern object
            Pattern r = Pattern.compile(regex);
            Matcher m = r.matcher(newPassword);
            if (!m.find())
                throw new Exception("Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character");
        }
    }
}
