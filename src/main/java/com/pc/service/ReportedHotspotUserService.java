package com.pc.service;

import com.pc.entities.ReportedHotspot;
import com.pc.entities.ReportedHotspotUser;
import com.pc.framework.AbstractService;
import com.pc.repositories.ReportedHotspotUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportedHotspotUserService extends AbstractService {
    @Autowired
    ReportedHotspotUserRepository repository;

    public void saveReportedHotspotUser(ReportedHotspotUser reportedHotspotUser) throws Exception {
        repository.save(reportedHotspotUser);
    }

    public void deleteReportedHotspotUser(ReportedHotspotUser reportedHotspotUser) throws Exception {
        repository.delete(reportedHotspotUser);
    }

    public void deleteReportedHotspotUserByID(Integer arg0) throws Exception {
        repository.deleteById(arg0);
    }

    public List<ReportedHotspotUser> findAllReportedHotspotUser() throws Exception {
        return repository.findAll();
    }

    public Page<ReportedHotspotUser> findAllReportedHotspotUser(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<ReportedHotspotUser> findAllReportedHotspotUser(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public ReportedHotspotUser findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    public ReportedHotspotUser findByReportedHotspotAndEmail(ReportedHotspot hotspot, String email) throws Exception {
        return repository.findByReportedHotspotAndEmail(hotspot, email);
    }

    public List<ReportedHotspotUser> findByEmail(String email) throws Exception {
        return repository.findByEmail(email);
    }

}
