package com.pc.service;

import com.pc.entities.Hotspot;
import com.pc.entities.HotspotCrimeType;
import com.pc.entities.HotspotSafetyTip;
import com.pc.entities.lookup.CrimeType;
import com.pc.entities.lookup.SafetyTip;
import com.pc.framework.AbstractService;
import com.pc.repositories.HotspotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class HotspotService extends AbstractService {
    @Autowired
    private HotspotRepository repository;

    @Autowired
    private HotspotCrimeTypeService hotspotCrimeTypeService;

    @Autowired
    private HotspotSafetyTipService hotspotSafetyTipService;

    public void saveHotspot(Hotspot hotspot, List<CrimeType> selectedCrimeTypeList, List<SafetyTip> safetyTipList) throws Exception {
        if (hotspot.getId() == null) {
            hotspot.setCreateDate(new Date());
        } else {
            hotspot.setLastUpdateDate(new Date());
        }

        repository.save(hotspot);

        if (selectedCrimeTypeList != null) {
            for (CrimeType crimeType : selectedCrimeTypeList) {
                HotspotCrimeType hotspotCrimeType = new HotspotCrimeType();
                hotspotCrimeType.setHotspot(hotspot);
                hotspotCrimeType.setCrimeType(crimeType);
                hotspotCrimeTypeService.saveHotspotCrimeType(hotspotCrimeType);
            }
        }

        if (safetyTipList != null) {
            for (SafetyTip safetyTip : safetyTipList) {
                HotspotSafetyTip hotspotSafetyTip = new HotspotSafetyTip();
                hotspotSafetyTip.setSafetyTip(safetyTip);
                hotspotSafetyTip.setHotspot(hotspot);
                hotspotSafetyTipService.saveHotspotSafetyTip(hotspotSafetyTip);
            }
        }
    }

    public void deleteHotspot(Hotspot hotspot) throws Exception {
        repository.delete(hotspot);
    }

    public void deleteHotspotByID(Integer arg0) throws Exception {
        repository.deleteById(arg0);
    }

    public List<Hotspot> findAllHotspot() throws Exception {
        return repository.findAll();
    }

    public List<Hotspot> findByActive(boolean active) throws Exception {
        return repository.findByActive(active);
    }

    public Page<Hotspot> findAllHotspot(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<Hotspot> findAllHotspot(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public Hotspot findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    public double countAll() throws Exception {
        return repository.count();
    }

    public List<Hotspot> findByLongitudeAndLatitude(String log, String lat) throws Exception {
        return repository.findByLongitudeAndLatitude(log, lat);
    }

}
