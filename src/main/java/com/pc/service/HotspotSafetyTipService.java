package com.pc.service;

import com.pc.entities.Hotspot;
import com.pc.entities.HotspotSafetyTip;
import com.pc.framework.AbstractService;
import com.pc.repositories.HotspotSafetyTipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class HotspotSafetyTipService extends AbstractService {
    @Autowired
    HotspotSafetyTipRepository repository;

    public void saveHotspotSafetyTip(HotspotSafetyTip hotspotSafetyTip) throws Exception {
        repository.save(hotspotSafetyTip);
    }

    public void deleteHotspotSafetyTip(HotspotSafetyTip hotspotSafetyTip) throws Exception {
        repository.delete(hotspotSafetyTip);
    }

    public void deleteHotspotSafetyTipByID(Integer arg0) throws Exception {
        repository.deleteById(arg0);
    }

    public List<HotspotSafetyTip> findAllHotspotSafetyTip() throws Exception {
        return repository.findAll();
    }

    public Page<HotspotSafetyTip> findAllHotspotSafetyTip(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<HotspotSafetyTip> findAllHotspotSafetyTip(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public HotspotSafetyTip findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    public void deleteAll(List<HotspotSafetyTip> hotspotSafetyTipList) throws Exception {
        repository.deleteAll(hotspotSafetyTipList);
    }

    public void deleteHotspotSafetyTipByHotspot(Hotspot hotspot) {
        try {
            repository.deleteHotspotSafetyTipByHotspot(hotspot);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<HotspotSafetyTip> findByHotspot(Hotspot hotspot) throws Exception {
        return repository.findByHotspot(hotspot);
    }


}
