package com.pc.service;

import com.pc.beans.Mail;
import com.pc.constants.AppConstants;
import com.pc.entities.ImageModel;
import com.pc.entities.User;
import com.pc.entities.UserRole;
import com.pc.entities.enums.TitleGenderEnum;
import com.pc.entities.lookup.Gender;
import com.pc.entities.lookup.Title;
import com.pc.framework.AbstractService;
import com.pc.mail.MailSender;
import com.pc.repositories.UserRepository;
import com.pc.service.lookup.GenderService;
import com.pc.service.lookup.RoleService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UserService extends AbstractService {

    private static final SimpleDateFormat sdfIdDate = new SimpleDateFormat("yyMMdd");
    @Autowired
    UserRepository repository;
    @Autowired
    MailSender mailSender;
    @Autowired
    ImagesService imagesService;

    @Autowired
    GenderService genderService;

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    RoleService roleService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void saveUser(User user) throws Exception {
        boolean isNew = false;
        if (user.getId() == null) {
            if (user.getPassword() == null) {
                user.setPassword(generatePassword());
            }
            isNew = true;
            String plainPass = user.getPassword();
            //Setting gender and date of birth
            user.setDob(sdfIdDate.parse(user.getIdNumber().substring(0, 6)));
            if (Integer.parseInt(user.getIdNumber().substring(6, 7)) > 4) {
                user.setGender(genderService.findByGenderName("Male"));
            } else {
                user.setGender(genderService.findByGenderName("Female"));
            }

            if(user.getTitle().getGenderType() != TitleGenderEnum.MaleAndFemale &&
                !user.getTitle().getGenderType().getFriendlyName().equalsIgnoreCase(user.getGender().getGenderName()) ){
                throw new Exception("Title and gender mismatch, please select a title that is associated with your gender");
            }

            /*
             * Setting Default Image
             * The image with an ID of 0 must be added
             * in the ImageModel table
             */
            ImageModel defaultImg = imagesService.getById(0L);
            ImageModel img = new ImageModel();
            img.setName(defaultImg.getName());
            img.setPic(defaultImg.getPic());
            img.setType(defaultImg.getType());
            imagesService.save(img);
            user.setUsername(user.getEmail());
            user.setImage(img);
            if (user.getPassword() != null) {
                String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
                user.setPassword(encodedPassword);
            } else {
                String encodedPassword = bCryptPasswordEncoder.encode(user.getIdNumber());
                user.setPassword(encodedPassword);
            }
            //Send login email notification
            try {
                registrationNotification(user, plainPass);
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("Sending Email Error: " + e.getMessage());
            }
            user.setCreateDate(new Date());
        } else {
            if (getCurrentUser() != null) {
                user.setLastUpdateUser(getCurrentUser());
            }
            user.setLastUpdateDate(new Date());
        }
        repository.save(user);
        if (isNew) {
            addGeneralUserRole(user);
        }
        user.setUsername(user.getEmail());


    }

    public void addGeneralUserRole(User user) throws Exception {
        UserRole userRole = new UserRole();
        userRole.setRole(roleService.findByCode("USER"));
        userRole.setUser(user);
        userRoleService.saveUserRole(userRole);
    }

    public void deleteUser(User User) throws Exception {
        repository.delete(User);
    }

    public User changePassword(String email, String password, String newPassword) throws Exception {
        User user = repository.getUserByEmail(email);
        if (user == null) {
            throw new Exception("User with email address: " + email
                + " is not registered on the system! If you typed in the correct email please contact support.");
        } else {
            if (!bCryptPasswordEncoder.matches(password.trim(), user.getPassword().trim())) {
                throw new Exception("Invalid password for user id: " + email);
            }

            user.setPassword(bCryptPasswordEncoder.encode(newPassword));
            repository.save(user);
        }
        return user;

    }

    public void resetPassword(User user, String newPassword) throws Exception {
        user.setPassword(bCryptPasswordEncoder.encode(newPassword));
        user.setChangePassword(false);
        repository.save(user);

    }

    public List<User> findAllUser() throws Exception {
        return repository.findAll();
    }

    public Page<User> findAllUser(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<User> findAllUser(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public User findByEmail(String email) throws Exception {
        return repository.findByEmail(email);
    }

    public User findByIdAndLoginToken(Long userId, String token) throws Exception {
        return repository.findByIdAndLoginToken(userId, token);
    }


    public User findByEmailAndIdNumberNot(String email, String idNumber) throws Exception {
        return repository.findByEmailAndIdNumberNot(email, idNumber);
    }

    public User getUserByIdNumber(String idNumber) throws Exception {
        return repository.getUserByIdNumber(idNumber);
    }

    public User getUserByEmail(String email) throws Exception {
        return repository.getUserByEmail(email);
    }


    public double countAll() throws Exception {
        return repository.count();
    }


    public void notifyUserNewPasswordEmail(String email) throws Exception {
        User ul = getUserByEmail(email);
        if (ul == null) {
            throw new Exception("User not registered");
        } else {
            notifyUserNewPassword(ul);
        }
    }


    /**
     * Notify a user of new password.
     *
     * @param u the u
     * @throws Exception the exception
     */
    public void notifyUserNewPassword(User u) throws Exception {
        //Generate password
        String pwd = generatePassword();
        u.setPassword(bCryptPasswordEncoder.encode(pwd));
        u.setChangePassword(true);
        repository.save(u);

        String welcome = "<p>Dear #NAME#,</p>" + "<br/>"
            + "<p>Your login details have been updated, below find your new login details</p>"

            + "<p><b>Username:</b> "+u.getEmail()+"</p>"
            + "<p><b>New password:</b> "+pwd+"</p>"

            + "<p>Note: You have to change it when you login.</p>"
            + "<p>Regards</p>"
            + "<p>Vehicle Crime Prevention Team</p>"
            + "<br/>";
        welcome = welcome.replaceAll("#NAME#", u.getName() + " " + u.getSurname());

        Mail mail = new Mail();

        mail.setContent(welcome);
        mail.setFrom(AppConstants.FROM_EMAIL);
        String[] to = {u.getEmail()};
        mail.setTo(to);
        mail.setSubject("New Login Details");
        mail.setCc(to);
        mailSender.sendHtmlEmil(mail);
    }


    /**
     * Registration Email
     */
    public void registrationNotification(User u, String plainPass) throws Exception {

        String welcome = "<p>Dear #NAME#,</p>"
            + "<p>Your Vehicle Crime Prevention account has been created </p>"
            + "<br/>"
            + "<p>Below, please find your login details: </p>"
            + "<p> <b>Username:</b> " + u.getEmail() + "</p>"
            + "<p> <b>Password:</b> " + plainPass + "</p>"
            + "<p>Regards</p>"
            + "<p>Vehicle Crime Prevention Team</p>"
            + "<br/>";
        welcome = welcome.replaceAll("#NAME#", u.getName() + " " + u.getSurname());

        Mail mail = new Mail();

        mail.setContent(welcome);
        mail.setFrom(AppConstants.FROM_EMAIL);
        String[] to = {u.getEmail()};
        mail.setTo(to);
        mail.setSubject("Vehicle Crime Prevention Registration");
        mail.setCc(to);
        mailSender.sendHtmlEmil(mail);
    }


    public String generatePassword() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                (random.nextFloat() * (rightLimit - leftLimit + 1));
            Random rand = new Random();
            int n = rand.nextInt(15) + 1;
            if (n > 10) {
                buffer.append(Character.toUpperCase((char) randomLimitedInt));
            } else {
                buffer.append((char) randomLimitedInt);
            }
        }
        String generatedString = buffer.toString();
        generatedString = generatedString.trim();
        System.out.println(generatedString);
        return generatedString;
    }

    public void saveProfileImage(User currentUser, FileUploadEvent event) throws Exception {

        if (currentUser.getImage() == null) {
            ImageModel imageModel = new ImageModel();
            imageModel.setName(event.getFile().getFileName().trim());
            imageModel.setType(event.getFile().getContentType());
            imageModel.setPic(event.getFile().getContents());
            imageModel = imagesService.save(imageModel);
            currentUser.setImage(imageModel);
            saveUser(currentUser);

        } else {
            ImageModel newImg = currentUser.getImage();
            newImg.setName(event.getFile().getFileName().trim());
            newImg.setType(event.getFile().getContentType());
            newImg.setPic(event.getFile().getContents());
            imagesService.save(newImg);
        }

    }

    public long countByTitle(Title title) {
        return repository.countByTitle(title);
    }

    public long countByGender(Gender gender) {
        return repository.countByGender(gender);
    }

}
