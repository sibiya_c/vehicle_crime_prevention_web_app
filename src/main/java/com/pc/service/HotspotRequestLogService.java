package com.pc.service;

import com.pc.entities.HotspotRequestLog;
import com.pc.framework.AbstractService;
import com.pc.repositories.HotspotRequestLogRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class HotspotRequestLogService extends AbstractService {

    @Autowired
    HotspotRequestLogRepository repository;

    public void saveHotspotRequestLog(HotspotRequestLog hotspotRequestLog) throws Exception {
        if (hotspotRequestLog.getId() == null) {
            hotspotRequestLog.setCreateDate(new Date());
        }
        repository.save(hotspotRequestLog);
    }

    public void deleteHotspotRequestLog(HotspotRequestLog hotspotRequestLog) throws Exception {
        repository.delete(hotspotRequestLog);
    }

    public void deleteHotspotRequestLogByID(Integer arg0) throws Exception {
        repository.deleteById(arg0);
    }

    public List<HotspotRequestLog> findAllHotspotRequestLog() throws Exception {
        return repository.findAll();
    }

    public Page<HotspotRequestLog> findAllHotspotRequestLog(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<HotspotRequestLog> findAllHotspotRequestLog(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public HotspotRequestLog findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    public double countAll() throws Exception {
        return repository.count();
    }


}
