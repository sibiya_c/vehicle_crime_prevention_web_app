package com.pc.service;

import com.pc.beans.Mail;
import com.pc.constants.AppConstants;
import com.pc.entities.Hotspot;
import com.pc.entities.HotspotCrimeType;
import com.pc.entities.ReportedHotspot;
import com.pc.entities.ReportedHotspotCrimeType;
import com.pc.entities.ReportedHotspotUser;
import com.pc.entities.lookup.AppConfig;
import com.pc.entities.lookup.CrimeType;
import com.pc.framework.AbstractService;
import com.pc.mail.MailSender;
import com.pc.repositories.ReportedHotspotRepository;
import com.pc.service.lookup.AppConfigService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ReportedHotspotService extends AbstractService {

    private static final Logger logger = LoggerFactory.getLogger(ReportedHotspotService.class);
    @Autowired
    private ReportedHotspotRepository repository;
    @Autowired
    private ReportedHotspotCrimeTypeService reportedHotspotCrimeTypeService;
    @Autowired
    private HotspotService hotspotService;
    @Autowired
    private AppConfigService appConfigService;
    @Autowired
    private ReportedHotspotUserService reportedHotspotUserService;
    @Autowired
    private HotspotCrimeTypeService hotspotCrimeTypeService;
    @Autowired
    private MailSender mailSender;

    public void saveReportedHotspot(ReportedHotspot reportedHotspot) throws Exception {
        repository.save(reportedHotspot);
    }

    public void saveReportedHotspot(ReportedHotspot reportedHotspot,
        List<CrimeType> selectedCrimeTypeList) throws Exception {
        //Checking if the hotspot does not exist
        AppConfig geofenceRadius = appConfigService.findByCode("GEOFENCE_RADIUS");
        if (geofenceRadius == null) {
            logger.error("GEOFENCE_RADIUS not configured");
            throw new Exception("Service unavailable");
        }
        double radius = Double.parseDouble(geofenceRadius.getValue());//Meters
        List<CrimeType> tempCrimeTypeList = selectedCrimeTypeList;
        List<Hotspot> hotspotList = hotspotService.findAllHotspot();
        for (Hotspot hotspot : hotspotList) {
            double distance = distance(Double.parseDouble(reportedHotspot.getLatitude()),
                Double.parseDouble(hotspot.getLatitude()),
                Double.parseDouble(reportedHotspot.getLongitude()),
                Double.parseDouble(hotspot.getLongitude()), 0, 0);
            if (distance <= radius) {
                logger.info("This reported hotspot ({}) is close to ",
                    reportedHotspot.getFormattedAddress(), hotspot.getFormattedAddress());

                List<HotspotCrimeType> hotspotCrimeTypes = hotspotCrimeTypeService
                    .findByHotspot(hotspot);
                if (hotspotCrimeTypes != null) {
                    hotspotCrimeTypes
                        .forEach(rhct -> tempCrimeTypeList.remove(rhct.getCrimeType()));
                }
                if (tempCrimeTypeList.isEmpty()) {
                    throw new Exception("This hotspot has been reported already");
                }

            }
        }

        //Checking if the user has not reported the same hotspot
        List<ReportedHotspotUser> reportedHotspotUserList = reportedHotspotUserService
            .findByEmail(reportedHotspot.getEmail());
        if (reportedHotspotUserList != null) {
            for (ReportedHotspotUser reportedHotspotUser : reportedHotspotUserList) {
                double distance = distance(Double.parseDouble(reportedHotspot.getLatitude()),
                    Double.parseDouble(reportedHotspotUser.getReportedHotspot().getLatitude()),
                    Double.parseDouble(reportedHotspot.getLongitude()),
                    Double.parseDouble(reportedHotspotUser.getReportedHotspot().getLongitude()), 0,
                    0);
                if (distance <= radius) {
                    List<ReportedHotspotCrimeType> reportedHotspotCrimeTypes = reportedHotspotCrimeTypeService
                        .findByReportedHotspot(reportedHotspotUser.getReportedHotspot());
                    if (reportedHotspotCrimeTypes != null) {
                        reportedHotspotCrimeTypes
                            .forEach(rhct -> tempCrimeTypeList.remove(rhct.getCrimeType()));
                    }
                    if (tempCrimeTypeList.isEmpty()) {
                        throw new Exception("You already reported this hotspot");
                    }
                }
            }
        }

        //Check if the hotspot is not reported already, if yes, then increment the number of total number reported
        List<ReportedHotspot> reportedHotspotList = repository.findAll();
        ReportedHotspot existingHotspot = null;
        if (reportedHotspotList != null && !reportedHotspotList.isEmpty()) {
            for (ReportedHotspot hotspot : reportedHotspotList) {
                double distance = distance(Double.parseDouble(reportedHotspot.getLatitude()),
                    Double.parseDouble(hotspot.getLatitude()),
                    Double.parseDouble(reportedHotspot.getLongitude()),
                    Double.parseDouble(hotspot.getLongitude()), 0, 0);
                if (distance <= radius) {
                    existingHotspot = hotspot;
                    List<ReportedHotspotCrimeType> existingReportedHotspotCrimeType = reportedHotspotCrimeTypeService
                        .findByReportedHotspot(hotspot);
                    if (existingReportedHotspotCrimeType != null) {
                        existingReportedHotspotCrimeType
                            .forEach(rhct -> selectedCrimeTypeList.remove(rhct.getCrimeType()));
                    }
                    break;
                }
            }
        }

        if (existingHotspot == null) {
            logger.info("Adding new hotspot...");
            reportedHotspot.setTotalNumberReported(1);
            if (reportedHotspot.getId() == null) {
                reportedHotspot.setCreateDate(new Date());
            } else {

                reportedHotspot.setLastUpdateDate(new Date());
            }
            reportedHotspot.setProcessed("NO");
            repository.save(reportedHotspot);
            saveCrimeTypes(reportedHotspot, selectedCrimeTypeList);
        } else {
            logger.info("Hotspot already exist, incrementing the total number of reported...");
            existingHotspot.setProcessed("NO");
            existingHotspot.setTotalNumberReported(existingHotspot.getTotalNumberReported() + 1);
            repository.save(existingHotspot);
            saveCrimeTypes(existingHotspot, selectedCrimeTypeList);
        }

        saveReporter(reportedHotspot);

        sendEmailToReporter(reportedHotspot);

        sendEmailAdmin(reportedHotspot, selectedCrimeTypeList);
    }


    public void sendEmailToReporter(ReportedHotspot reportedHotspot) throws Exception {

        String welcome = "<p>Dear #NAME#,</p>"
            + "<p>" + reportedHotspot.getFormattedAddress()
            + " has been reported as a vehicle crime hotspot </p>"
            + "<p>Thank you for helping to keep South Africans safe on the road</p>"
            + "<br/>"
            + "<p>Regards</p>"
            + "<p>Vehicle Crime Prevention Team</p>"
            + "<br/>";
        welcome = welcome.replaceAll("#NAME#", reportedHotspot.getFullName());

        Mail mail = new Mail();

        mail.setContent(welcome);
        mail.setFrom(AppConstants.FROM_EMAIL);
        String[] to = {reportedHotspot.getEmail()};
        mail.setTo(to);
        mail.setSubject("Vehicle Crime Hotspot");
        mail.setCc(to);
        mailSender.sendHtmlEmil(mail);
    }

    public void sendEmailAdmin(ReportedHotspot reportedHotspot,
        List<CrimeType> selectedCrimeTypeList) throws Exception {

        List<AppConfig> adminEmails = appConfigService.findListByCode("HOTSPOT_REPORT_EMAIL");

        String additionalInfo = reportedHotspot.getAdditionalInfo();
        if (additionalInfo == null || additionalInfo.isEmpty()) {
            additionalInfo = "N/A";
        }

        StringBuilder crimeType = new StringBuilder("<ul>");
        selectedCrimeTypeList
            .forEach(cr -> crimeType.append(" <li>" + cr.getDescription() + "</li>"));
        crimeType.append("</ul>");

        if (adminEmails != null && !adminEmails.isEmpty()) {
            List<String> emails = new ArrayList<>();
            adminEmails.forEach(cr -> emails.add(cr.getValue()));

            String welcome = "<p>Dear #NAME#,</p>"
                + "<p>Vehicle crime hotspot has been reported, below please find the hotspot details: </p>"

                + "<p><b>Address: </b> " + reportedHotspot.getFormattedAddress() + "</p>"
                + "<p><b>Crime Types: </b> <br/>" + crimeType + "</p>"
                + "<p><b>Number of reports at this location: </b> " + reportedHotspot
                .getTotalNumberReported()
                + "<p><b>Additional info: </b> " + additionalInfo + "</p>"
                + "<br/>"

                + "<p>Regards</p>"
                + "<p>Vehicle Crime Prevention Team</p>"
                + "<br/>";
            welcome = welcome.replaceAll("#NAME#", "System admin");

            Mail mail = new Mail();

            mail.setContent(welcome);
            mail.setFrom(AppConstants.FROM_EMAIL);
            String[] to = emails.toArray(new String[0]);
            mail.setTo(to);
            mail.setSubject("Vehicle Crime Hotspot");
            mail.setCc(to);
            mailSender.sendHtmlEmil(mail);
        } else {

        }
    }


    private void saveCrimeTypes(ReportedHotspot reportedHotspot,
        List<CrimeType> selectedCrimeTypeList) throws Exception {
        if (selectedCrimeTypeList != null) {
            for (CrimeType crimeType : selectedCrimeTypeList) {
                ReportedHotspotCrimeType reportedHotspotCrimeType = new ReportedHotspotCrimeType();
                reportedHotspotCrimeType.setReportedHotspot(reportedHotspot);
                reportedHotspotCrimeType.setCrimeType(crimeType);
                reportedHotspotCrimeTypeService
                    .saveReportedHotspotCrimeType(reportedHotspotCrimeType);
            }
        }
    }

    private void saveReporter(ReportedHotspot reportedHotspot) throws Exception {
        ReportedHotspotUser reportedHotspotUser = new ReportedHotspotUser();
        reportedHotspotUser.setReportedHotspot(reportedHotspot);
        reportedHotspotUser.setEmail(reportedHotspot.getEmail());
        reportedHotspotUser.setFullName(reportedHotspot.getFullName());
        reportedHotspotUserService.saveReportedHotspotUser(reportedHotspotUser);
    }

    public void deleteReportedHotspot(ReportedHotspot reportedHotspot) throws Exception {
        repository.delete(reportedHotspot);
    }

    public void deleteReportedHotspotByID(Integer arg0) throws Exception {
        repository.deleteById(arg0);
    }

    public List<ReportedHotspot> findAllReportedHotspot() throws Exception {
        return repository.findAll();
    }

    public Page<ReportedHotspot> findAllReportedHotspot(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<ReportedHotspot> findAllReportedHotspot(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public ReportedHotspot findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    public double countAll() throws Exception {
        return repository.count();
    }


    public void processReportedHotspots() {
        logger.info("PROCESSING CRIME HOTSPOTS");
        try {
            List<ReportedHotspot> reportedHotspotList = repository.findAll();
            List<Hotspot> hotspotList = hotspotService.findAllHotspot();
            List<ReportedHotspot> newHotspotList = new ArrayList<>();
            appConfigService.findByCode("GEOFENCE_RADIUS");
            double radius = Double
                .parseDouble(appConfigService.findByCode("GEOFENCE_RADIUS").getValue());//Meters

            //Finding the reported hotspots that are not in Hotspot table
            reportedHotspotList.forEach(reportedHotspot -> {
                boolean exist = false;
                for (Hotspot hotspot : hotspotList) {
                    double distance = distance(Double.parseDouble(reportedHotspot.getLatitude()),
                        Double.parseDouble(hotspot.getLatitude()),
                        Double.parseDouble(reportedHotspot.getLongitude()),
                        Double.parseDouble(hotspot.getLongitude()), 0, 0);
                    if (distance <= radius) {
                        logger.info("This reported hotspot ({}) is close to ",
                            reportedHotspot.getFormattedAddress(), hotspot.getFormattedAddress());
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    newHotspotList.add(reportedHotspot);
                }
            });

            logger.info("New hotspots size: {}", newHotspotList.size());

            List<ReportedHotspot> duplicatedNewHotspotList = new ArrayList<>();

            newHotspotList.forEach(hotspot -> {
                if (isDuplicated(newHotspotList, hotspot, radius)) {
                    duplicatedNewHotspotList.add(hotspot);
                }
            });

            logger.info("Duplicated hotspots size: {}", duplicatedNewHotspotList.size());
            newHotspotList.removeAll(duplicatedNewHotspotList);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean isDuplicated(List<ReportedHotspot> hotspotList, ReportedHotspot hotspot,
        double radius) {
        boolean duplicate = false;

        for (ReportedHotspot hp : hotspotList) {
            if (!hotspot.equals(hp)) {
                double distance = distance(Double.parseDouble(hp.getLatitude()),
                    Double.parseDouble(hotspot.getLatitude()),
                    Double.parseDouble(hp.getLongitude()),
                    Double.parseDouble(hotspot.getLongitude()), 0, 0);
                if (distance <= radius) {
                    duplicate = true;
                    break;
                }
            }
        }

        return duplicate;

    }

    /**
     * Calculate distance between two points in latitude and longitude taking into account height
     * difference. If you are not interested in height difference pass 0.0. Uses Haversine method as
     * its base.
     * <p>
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters el2 End altitude in
     * meters
     *
     * @returns Distance in Meters
     */
    public double distance(double lat1, double lat2, double lon1, double lon2, double el1,
        double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
            + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);
        double theDistance = Math.sqrt(distance);

        return theDistance;
    }


}
