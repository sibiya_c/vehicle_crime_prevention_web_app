package com.pc.service.lookup;

import com.pc.entities.lookup.Gender;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.GenderRepository;
import com.pc.service.UserService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class GenderService extends AbstractService {

    @Autowired
    private GenderRepository repository;

    @Autowired
    private UserService userService;

    public void saveGender(Gender gender) throws Exception {

        if (gender.getId() == null) {
            checkIfExist(gender.getGenderName());
            gender.setCreateDate(new Date());

        } else {
            if (getCurrentUser() != null) {
                gender.setLastUpdateUser(getCurrentUser());
            }
            gender.setLastUpdateDate(new Date());
        }

        repository.saveAndFlush(gender);

    }

    public Gender findByGenderName(String name) throws Exception {
        return repository.findByGenderName(name);
    }

    public void deleteGender(Gender gender) throws Exception {
        checkIfInUse(gender);
        repository.delete(gender);
    }

    public List<Gender> findAllGender() throws Exception {
        return repository.findAll();
    }

    public Page<Gender> findAllGender(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<Gender> findAllGender(Sort s) throws Exception {
        return repository.findAll(s);
    }


    public Gender findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    private void checkIfExist(String desc) throws Exception {
        Gender gender = repository.findByGenderName(desc);
        if (gender != null) {
            throw new Exception("Gender already exist");
        }
    }

    private void checkIfInUse(Gender gender) throws Exception {
        long count = userService.countByGender(gender);
        if (count > 0) {
            throw new Exception("This gender cannot be deleted because it's being used");
        }
    }

}
