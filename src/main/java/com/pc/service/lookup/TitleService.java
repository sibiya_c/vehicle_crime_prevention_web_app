package com.pc.service.lookup;

import com.pc.entities.lookup.Title;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.TitleRepository;
import com.pc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TitleService extends AbstractService {
    @Autowired
    private TitleRepository repository;
    @Autowired
    private UserService userService;

    public void saveTitle(Title title) throws Exception {

        if (title.getId() == null) {
            checkIfExist(title.getDescription());
            title.setCreateDate(new Date());
        } else {
            if (getCurrentUser() != null) {
                title.setLastUpdateUser(getCurrentUser());
            }
            title.setLastUpdateDate(new Date());
        }
        repository.save(title);
    }

    public void deleteTitle(Title title) throws Exception {
        checkIfInUse(title);
        repository.delete(title);
    }

    public List<Title> findAllTitle() throws Exception {
        return repository.findAll();
    }

    public Page<Title> findAllTitle(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<Title> findAllTitle(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public Object findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    private void checkIfExist(String desc) throws Exception {
        List<Title> list = repository.findByDescription(desc);
        if (list != null && list.size() > 0) {
            throw new Exception("Title already exist");
        }
    }

    private void checkIfInUse(Title title) throws Exception {
        long count = userService.countByTitle(title);
        if (count > 0) {
            throw new Exception("This title cannot be deleted because it's being used");
        }
    }


}
