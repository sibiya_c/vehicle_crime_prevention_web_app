package com.pc.service.lookup;

import com.pc.entities.lookup.SafetyTip;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.SafetyTipRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SafetyTipService extends AbstractService {

    @Autowired
    SafetyTipRepository repository;

    public void saveSafetyTip(SafetyTip safetyTip) throws Exception {
        if (safetyTip.getId() == null) {
            safetyTip.setCreateDate(new Date());
        } else {

            if (getCurrentUser() != null) {
                safetyTip.setLastUpdateUser(getCurrentUser());
            }
            safetyTip.setLastUpdateDate(new Date());
        }
        repository.save(safetyTip);
    }

    public void deleteSafetyTip(SafetyTip safetyTip) throws Exception {
        repository.delete(safetyTip);
    }

    public void deleteSafetyTipByID(Integer arg0) throws Exception {
        repository.deleteById(arg0);
    }

    public List<SafetyTip> findAllSafetyTip() throws Exception {
        return repository.findAll();
    }

    public Page<SafetyTip> findAllSafetyTip(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<SafetyTip> findAllSafetyTip(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public SafetyTip findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    public List<SafetyTip> findByDescriptionStartingWith(String description) throws Exception {
        return repository.findByDescriptionStartingWith(description);
    }


}
