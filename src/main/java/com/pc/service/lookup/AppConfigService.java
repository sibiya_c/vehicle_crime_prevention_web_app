package com.pc.service.lookup;

import com.pc.entities.lookup.AppConfig;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.AppConfigRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class AppConfigService extends AbstractService {

    @Autowired
    AppConfigRepository repository;

    public void saveAppConfig(AppConfig appConfig) throws Exception {
        if (appConfig.getId() == null) {
            appConfig.setCreateDate(new Date());
        } else {

            if (getCurrentUser() != null) {
                appConfig.setLastUpdateUser(getCurrentUser());
            }
            appConfig.setLastUpdateDate(new Date());
        }
        repository.save(appConfig);
    }

    public void deleteAppConfig(AppConfig appConfig) throws Exception {
        repository.delete(appConfig);
    }

    public void deleteAppConfigByID(Integer arg0) throws Exception {
        repository.deleteById(arg0);
    }

    public List<AppConfig> findAllAppConfig() throws Exception {
        return repository.findAll();
    }

    public Page<AppConfig> findAllAppConfig(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<AppConfig> findAllAppConfig(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public AppConfig findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    public AppConfig findByCode(String code) throws Exception {
        return repository.findByCode(code).get(0);
    }

    public List<AppConfig> findListByCode(String code) throws Exception {
        return repository.findByCode(code);
    }


}
