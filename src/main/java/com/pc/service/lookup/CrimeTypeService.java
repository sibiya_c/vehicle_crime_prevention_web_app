package com.pc.service.lookup;

import com.pc.entities.lookup.CrimeType;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.CrimeTypeRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class CrimeTypeService extends AbstractService {

    @Autowired
    CrimeTypeRepository repository;

    public void saveCrimeType(CrimeType crimeType) throws Exception {
        if (crimeType.getId() == null) {
            crimeType.setCreateDate(new Date());
        } else {

            if (getCurrentUser() != null) {
                crimeType.setLastUpdateUser(getCurrentUser());
            }
            crimeType.setLastUpdateDate(new Date());
        }
        repository.save(crimeType);
    }

    public void deleteCrimeType(CrimeType crimeType) throws Exception {
        repository.delete(crimeType);
    }

    public void deleteCrimeTypeByID(Integer arg0) throws Exception {
        repository.deleteById(arg0);
    }

    public List<CrimeType> findAllCrimeType() throws Exception {
        return repository.findAll();
    }

    public Page<CrimeType> findAllCrimeType(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<CrimeType> findAllCrimeType(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public CrimeType findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    public List<CrimeType> findByDescriptionStartingWith(String description) throws Exception {
        return repository.findByDescriptionStartingWith(description);
    }


}
