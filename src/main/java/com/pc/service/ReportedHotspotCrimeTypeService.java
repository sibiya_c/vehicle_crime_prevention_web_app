package com.pc.service;

import java.util.List;

import com.pc.entities.ReportedHotspot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;

import com.pc.entities.ReportedHotspotCrimeType;
import com.pc.repositories.ReportedHotspotCrimeTypeRepository;
import com.pc.framework.AbstractService;

@Service
public class ReportedHotspotCrimeTypeService extends AbstractService {
    @Autowired
    ReportedHotspotCrimeTypeRepository repository;

    public void saveReportedHotspotCrimeType(ReportedHotspotCrimeType reportedHotspotCrimeType) throws Exception {
        repository.save(reportedHotspotCrimeType);
    }

    public void deleteReportedHotspotCrimeType(ReportedHotspotCrimeType reportedHotspotCrimeType) throws Exception {
        repository.delete(reportedHotspotCrimeType);
    }

    public void deleteReportedHotspotCrimeTypeByID(Integer arg0) throws Exception {
        repository.deleteById(arg0);
    }

    public List<ReportedHotspotCrimeType> findAllReportedHotspotCrimeType() throws Exception {
        return repository.findAll();
    }

    public Page<ReportedHotspotCrimeType> findAllReportedHotspotCrimeType(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<ReportedHotspotCrimeType> findAllReportedHotspotCrimeType(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public ReportedHotspotCrimeType findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }


    public List<ReportedHotspotCrimeType> findByReportedHotspot(ReportedHotspot hotspot) throws Exception {
        return repository.findByReportedHotspot(hotspot);
    }



}
