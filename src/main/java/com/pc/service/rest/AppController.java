package com.pc.service.rest;

import com.pc.beans.HotspotBean;
import com.pc.beans.HotspotDetailBean;
import com.pc.beans.UserBean;
import com.pc.entities.Hotspot;
import com.pc.entities.HotspotCrimeType;
import com.pc.entities.HotspotRequestLog;
import com.pc.entities.HotspotSafetyTip;
import com.pc.entities.User;
import com.pc.entities.lookup.AppConfig;
import com.pc.service.HotspotCrimeTypeService;
import com.pc.service.HotspotRequestLogService;
import com.pc.service.HotspotSafetyTipService;
import com.pc.service.HotspotService;
import com.pc.service.UserService;
import com.pc.service.lookup.AppConfigService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/*@CrossOrigin(origins = "http://localhost", maxAge = 3600)*/
@RestController
public class AppController {

    private static final Logger logger = LoggerFactory.getLogger(AppController.class);
    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private HotspotRequestLogService hotspotRequestLogService;

    @Autowired
    private HotspotService hotspotService;

    @Autowired
    private HotspotCrimeTypeService hotspotCrimeTypeService;

    @Autowired
    private HotspotSafetyTipService hotspotSafetyTipService;

    @Autowired
    private AppConfigService appConfigService;

    @RequestMapping(path = "/rest/validate_login/{email}/{password}", method = RequestMethod.GET)
    @ResponseBody
    public UserBean validateLogin(@PathVariable("email") String email,
        @PathVariable("password") String password) {
        logger.info("Calling validate_login. email: {}", email);
        User user;
        UserBean userBean = new UserBean();
        userBean.setSuccess(false);
        try {
            user = userService.getUserByEmail(email);
            if (user != null) {
                boolean correctPass = bCryptPasswordEncoder.matches(password, user.getPassword());
                if (!correctPass) {
                    user = null;
                    userBean.setSuccess(false);
                    userBean.setErrorMessage("Invalid login details");
                }
            }
            if (user != null) {

                userBean.setToken(UUID.randomUUID().toString());
                BeanUtils.copyProperties(userBean, user);

                userBean.setSuccess(true);

                if (user.getTitle() != null) {
                    userBean.setTitle(user.getTitle().getDescription());
                }

                if (user.getGender() != null) {
                    userBean.setGender(user.getGender().getGenderName());
                }

                user.setLoginToken(userBean.getToken());
                user.setLastLoginDate(new Date());
                userService.saveUser(user);
            }
        } catch (Exception e) {
            userBean.setSuccess(false);
            userBean.setErrorMessage("Invalid login details");
            logger.error("Error: ", e);
        }

        logger.info("Calling validate_login. Response: {}", userBean);
        return userBean;
    }


    @RequestMapping(value = "/rest/find_all_hotspots/{user_id}/{token}", method = RequestMethod.GET)
    @ResponseBody
    public HotspotDetailBean findAllHotspots(@PathVariable("user_id") String userId,
        @PathVariable("token") String token) {
        logger.info("Calling find_all_hotspots. UserId: {}, Token: {}", userId, token);
        HotspotDetailBean hotspotDetailBean = new HotspotDetailBean();
        List<HotspotBean> hotspotBeanList = new ArrayList<>();
        try {
            User user = userService
                .findByIdAndLoginToken(Long.parseLong(userId), token);
            if (user == null) {
                hotspotDetailBean.setSuccess(false);
                hotspotDetailBean.setErrorMessage("Invalid user ID and token combination");
            } else {
                AppConfig geofenceRadius = appConfigService.findByCode("GEOFENCE_RADIUS");
                if (geofenceRadius == null) {
                    logger.error("GEOFENCE_RADIUS not configured");
                    throw new Exception("Service unavailable");
                }
                hotspotDetailBean.setRadius(geofenceRadius.getValue());
                List<Hotspot> hotspotList = hotspotService.findAllHotspot();

                if (hotspotList != null && !hotspotList.isEmpty()) {
                    for (Hotspot hotspot : hotspotList) {
                        hotspotBeanList.add(buildHotspotBean(hotspot));
                    }
                }

                hotspotDetailBean.setHotspotBeanList(hotspotBeanList);
                hotspotDetailBean.setErrorMessage(null);
                hotspotDetailBean.setSuccess(true);
            }
        } catch (Exception e) {
            hotspotDetailBean.setErrorMessage("Service unavailable");
            logger.error("Error: ", e);
        } finally {
            try {
                hotspotRequestLogService
                    .saveHotspotRequestLog(buildHotspotReqLog(userId, token, hotspotDetailBean));
            } catch (Exception e) {
                logger.error("Error when saving logs: ", e);
            }
        }
        logger.info("Calling find_all_hotspots. Response: {}", hotspotDetailBean);
        return hotspotDetailBean;
    }

    private HotspotBean buildHotspotBean(Hotspot hotspot) {
        HotspotBean hotspotBean = null;
        try {
            hotspotBean = new HotspotBean();
            hotspotBean.setLatitude(Double.parseDouble(hotspot.getLatitude()));
            hotspotBean.setLongitude(Double.parseDouble(hotspot.getLongitude()));
            hotspotBean.setAddress(hotspot.getFormattedAddress());
            hotspotBean.setAdditionalInfo(hotspot.getAdditionalInfo());
            hotspotBean.setId(hotspot.getId());
            List<String> crimeTypeList = new ArrayList<>();
            List<String> safetyTipsList = new ArrayList<>();

            List<HotspotCrimeType> hotspotCrimeTypeList = hotspotCrimeTypeService
                .findByHotspot(hotspot);
            List<HotspotSafetyTip> hotspotSafetyTipList = hotspotSafetyTipService
                .findByHotspot(hotspot);

            if (hotspotCrimeTypeList != null) {
                hotspotCrimeTypeList.forEach(hotspotCrimeType -> crimeTypeList
                    .add(hotspotCrimeType.getCrimeType().getDescription()));
            }

            if (hotspotSafetyTipList != null) {
                hotspotSafetyTipList
                    .forEach(hst -> safetyTipsList.add(hst.getSafetyTip().getDescription()));
            }

            hotspotBean.setSafetyTipsList(safetyTipsList);
            hotspotBean.setCrimeTypeList(crimeTypeList);
        } catch (Exception e) {
            logger.error("Error: ", e);
        }
        return hotspotBean;
    }

    private HotspotRequestLog buildHotspotReqLog(String id, String token,
        HotspotDetailBean hotspotDetailBean) {
        HotspotRequestLog requestLog = new HotspotRequestLog();
        requestLog.setUserId(id);
        requestLog.setToken(token);
        requestLog.setSuccess(hotspotDetailBean.isSuccess());
        requestLog.setErrorMessage(hotspotDetailBean.getErrorMessage());
        return requestLog;
    }


}
