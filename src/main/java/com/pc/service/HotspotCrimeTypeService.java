package com.pc.service;

import com.pc.entities.Hotspot;
import com.pc.entities.HotspotCrimeType;
import com.pc.framework.AbstractService;
import com.pc.repositories.HotspotCrimeTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class HotspotCrimeTypeService extends AbstractService {
    @Autowired
    HotspotCrimeTypeRepository repository;

    public void saveHotspotCrimeType(HotspotCrimeType hotspotCrimeType) throws Exception {
        repository.save(hotspotCrimeType);
    }

    public void deleteHotspotCrimeType(HotspotCrimeType hotspotCrimeType) throws Exception {
        repository.delete(hotspotCrimeType);
    }

    public void deleteHotspotCrimeTypeByID(Integer arg0) throws Exception {
        repository.deleteById(arg0);
    }

    public List<HotspotCrimeType> findAllHotspotCrimeType() throws Exception {
        return repository.findAll();
    }

    public Page<HotspotCrimeType> findAllHotspotCrimeType(Pageable p) throws Exception {
        return repository.findAll(p);
    }

    public List<HotspotCrimeType> findAllHotspotCrimeType(Sort s) throws Exception {
        return repository.findAll(s);
    }

    public HotspotCrimeType findById(Long parseLong) throws Exception {
        return repository.findById(parseLong);
    }

    public void deleteAll(List<HotspotCrimeType> hotspotCrimeTypeList) throws Exception {
        repository.deleteAll(hotspotCrimeTypeList);
    }

    public void deleteHotspotCrimeTypesByHotspot(Hotspot hotspot) {
        try {
            repository.deleteHotspotCrimeTypesByHotspot(hotspot);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<HotspotCrimeType> findByHotspot(Hotspot hotspot) throws Exception {
        return repository.findByHotspot(hotspot);
    }


}
