package com.pc.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pc.beans.AddressDetails;
import com.pc.entities.enums.GeoCodeType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class GeoCodeClient {
    private static final Logger logger = LogManager.getLogger(GeoCodeClient.class);
    private static final String GEOCODING_RESOURCE = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    private static final String GEOCODING_REVERSE = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";


    public AddressDetails getGoogleGeoCodeResponse(String searchData, String apiKey, GeoCodeType type) {
        AddressDetails addressDetails = new AddressDetails();

        try {
            GoogleGeoCodeResponse result = new GoogleGeoCodeResponse();
            String strUrl = null;

            if (type == GeoCodeType.AddressToCoordinates) {
                strUrl = GEOCODING_RESOURCE + URLEncoder.encode(searchData, "UTF-8") + "+CA&key=" + apiKey;
            } else {
                strUrl = GEOCODING_REVERSE + searchData + "+&key=" + apiKey;
            }

            result = (GoogleGeoCodeResponse) convertJsonToObject(callGetMethod(searchData, apiKey, strUrl), result);
            logger.info("RESULTS: {}", result);

            for (results resultResult : result.getResults()) {
                addressDetails.setStatus(result.getStatus());
                addressDetails.setFormattedAddress(resultResult.getFormatted_address());
                addressDetails.setLat(resultResult.getGeometry().getLocation().getLat());
                addressDetails.setLng(resultResult.getGeometry().getLocation().getLng());
                break;
            }
        } catch (Exception e) {
            logger.error("Error: ", e);
        }
        return addressDetails;
    }

    private String callGetMethod(String address, String apiKey, String strUrl) {

        String json = null;
        try {

            logger.debug("Calling Get Request [URL: {}]", strUrl);

            URL url = new URL(strUrl);
            String readLine;
            HttpURLConnection conection = (HttpURLConnection) url.openConnection();
            conection.setRequestMethod("GET");

            int responseCode = conection.getResponseCode();
            logger.debug("POST Response Code: {}, POST Response Message: {}", responseCode, conection.getResponseMessage());
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conection.getInputStream()));
                StringBuffer stringBuffer = new StringBuffer();
                while ((readLine = in.readLine()) != null) {
                    stringBuffer.append(readLine);
                }
                in.close();
                json = stringBuffer.toString();
                logger.info("JSON: {}", json);
            }
        } catch (Exception e) {
            logger.error("ERROR: ", e);
        }

        return json;

    }


    private Object convertJsonToObject(String json, Object objectClass) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(json, objectClass.getClass());
        } catch (JsonProcessingException e) {
            logger.error("Error when converting Json to object: ", e);
            return null;
        }
    }

}