package com.pc.client;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Arrays;

public class GoogleGeoCodeResponse {

    @JsonIgnore
    private plus_code plus_code;
    public String status;
    public results[] results;

    public GoogleGeoCodeResponse() {

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public com.pc.client.results[] getResults() {
        return results;
    }

    public void setResults(com.pc.client.results[] results) {
        this.results = results;
    }


    public com.pc.client.plus_code getPlus_code() {
        return plus_code;
    }

    public void setPlus_code(com.pc.client.plus_code plus_code) {
        this.plus_code = plus_code;
    }

    @Override
    public String toString() {
        return "GoogleGeoCodeResponse{" +
                "plus_code=" + plus_code +
                ", status='" + status + '\'' +
                ", results=" + Arrays.toString(results) +
                '}';
    }
}

class results {
    public String formatted_address;
    public geometry geometry;
    public String[] types;
    public address_component[] address_components;
    public String place_id;
    public plus_code plus_code;

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public com.pc.client.geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(com.pc.client.geometry geometry) {
        this.geometry = geometry;
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    public address_component[] getAddress_components() {
        return address_components;
    }

    public void setAddress_components(address_component[] address_components) {
        this.address_components = address_components;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public com.pc.client.plus_code getPlus_code() {
        return plus_code;
    }

    public void setPlus_code(com.pc.client.plus_code plus_code) {
        this.plus_code = plus_code;
    }

    @Override
    public String toString() {
        return "results{" +
                "formatted_address='" + formatted_address + '\'' +
                ", geometry=" + geometry +
                ", types=" + Arrays.toString(types) +
                ", address_components=" + Arrays.toString(address_components) +
                ", place_id='" + place_id + '\'' +
                ", plus_code=" + plus_code +
                '}';
    }
}

class plus_code {
    public String compound_code;
    public String global_code;

    public String getCompound_code() {
        return compound_code;
    }

    public void setCompound_code(String compound_code) {
        this.compound_code = compound_code;
    }

    public String getGlobal_code() {
        return global_code;
    }

    public void setGlobal_code(String global_code) {
        this.global_code = global_code;
    }

    @Override
    public String toString() {
        return "plus_code{" +
                "compound_code='" + compound_code + '\'' +
                ", global_code='" + global_code + '\'' +
                '}';
    }
}

class geometry {
    public bounds bounds;
    public String location_type;
    public location location;
    public bounds viewport;

    public com.pc.client.bounds getBounds() {
        return bounds;
    }

    public void setBounds(com.pc.client.bounds bounds) {
        this.bounds = bounds;
    }

    public String getLocation_type() {
        return location_type;
    }

    public void setLocation_type(String location_type) {
        this.location_type = location_type;
    }

    public com.pc.client.location getLocation() {
        return location;
    }

    public void setLocation(com.pc.client.location location) {
        this.location = location;
    }

    public com.pc.client.bounds getViewport() {
        return viewport;
    }

    public void setViewport(com.pc.client.bounds viewport) {
        this.viewport = viewport;
    }

    @Override
    public String toString() {
        return "geometry{" +
                "bounds=" + bounds +
                ", location_type='" + location_type + '\'' +
                ", location=" + location +
                ", viewport=" + viewport +
                '}';
    }
}

class bounds {

    public location northeast;
    public location southwest;

    public location getNortheast() {
        return northeast;
    }

    public void setNortheast(location northeast) {
        this.northeast = northeast;
    }

    public location getSouthwest() {
        return southwest;
    }

    public void setSouthwest(location southwest) {
        this.southwest = southwest;
    }

    @Override
    public String toString() {
        return "bounds{" +
                "northeast=" + northeast +
                ", southwest=" + southwest +
                '}';
    }
}

class location {
    public String lat;
    public String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "location{" +
                "lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                '}';
    }
}

class address_component {
    public String long_name;
    public String short_name;
    public String[] types;

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "address_component{" +
                "long_name='" + long_name + '\'' +
                ", short_name='" + short_name + '\'' +
                ", types=" + Arrays.toString(types) +
                '}';
    }
}