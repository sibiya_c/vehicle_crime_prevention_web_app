package com.pc.repositories;


import com.pc.entities.Hotspot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotspotRepository extends JpaRepository<Hotspot, Integer> {

    Hotspot findById(Long parseLong);

    List<Hotspot> findByActive(boolean active);

    List<Hotspot> findByLongitudeAndLatitude(String log,String lat);

}
