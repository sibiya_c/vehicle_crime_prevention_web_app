package com.pc.repositories;


import com.pc.entities.Hotspot;
import com.pc.entities.HotspotCrimeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotspotCrimeTypeRepository extends JpaRepository<HotspotCrimeType, Integer> {

    HotspotCrimeType findById(Long parseLong);

    long deleteHotspotCrimeTypesByHotspot(Hotspot hotspot);

    List<HotspotCrimeType> findByHotspot(Hotspot hotspot);
}
