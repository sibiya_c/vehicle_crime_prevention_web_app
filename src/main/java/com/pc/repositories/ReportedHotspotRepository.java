package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.ReportedHotspot;

@Repository
public interface ReportedHotspotRepository extends JpaRepository<ReportedHotspot, Integer> 
{
	
	public ReportedHotspot findById(Long parseLong);
	//public List<ReportedHotspot> findByDescriptionStartingWith(String description);
}
