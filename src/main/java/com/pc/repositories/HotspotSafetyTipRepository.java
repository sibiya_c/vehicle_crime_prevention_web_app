package com.pc.repositories;


import com.pc.entities.Hotspot;
import com.pc.entities.HotspotSafetyTip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotspotSafetyTipRepository extends JpaRepository<HotspotSafetyTip, Integer> {

    HotspotSafetyTip findById(Long parseLong);

    long deleteHotspotSafetyTipByHotspot(Hotspot hotspot);

    List<HotspotSafetyTip> findByHotspot(Hotspot hotspot);
}
