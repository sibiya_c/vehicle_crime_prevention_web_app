package com.pc.repositories;


import com.pc.entities.ReportedHotspot;
import com.pc.entities.ReportedHotspotUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportedHotspotUserRepository extends JpaRepository<ReportedHotspotUser, Integer> {

    ReportedHotspotUser findById(Long parseLong);

    ReportedHotspotUser findByReportedHotspotAndEmail(ReportedHotspot hotspot, String email);

    List<ReportedHotspotUser> findByEmail(String email);
}
