package com.pc.repositories.lookup;

import com.pc.entities.lookup.Title;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TitleRepository extends JpaRepository<Title, Integer> {

    Title findById(Long parseLong);

    List<Title> findByDescription(String desc);

}
