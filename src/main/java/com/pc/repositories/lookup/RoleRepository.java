package com.pc.repositories.lookup;

import com.pc.entities.lookup.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findById(Long id);

    Role findByCode(String code);

    List<Role> findByDescription(String desc);

}
