package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.CrimeType;

@Repository
public interface CrimeTypeRepository extends JpaRepository<CrimeType, Integer> 
{
	
	public CrimeType findById(Long parseLong);
	public List<CrimeType> findByDescriptionStartingWith(String description);
}
