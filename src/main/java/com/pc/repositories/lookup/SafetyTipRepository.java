package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.SafetyTip;

@Repository
public interface SafetyTipRepository extends JpaRepository<SafetyTip, Integer> 
{
	
	public SafetyTip findById(Long parseLong);
	public List<SafetyTip> findByDescriptionStartingWith(String description);
}
