package com.pc.repositories.lookup;


import com.pc.entities.lookup.AppConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppConfigRepository extends JpaRepository<AppConfig, Integer> {

    AppConfig findById(Long parseLong);

    List<AppConfig> findByCode(String code);
}
