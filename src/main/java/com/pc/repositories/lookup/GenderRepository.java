package com.pc.repositories.lookup;

import com.pc.entities.lookup.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface GenderRepository extends JpaRepository<Gender, Integer> {

    Gender findById(Long parseLong);

    Gender findByGenderName(String name);

}
