package com.pc.repositories;


import java.util.List;

import com.pc.entities.ReportedHotspot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.ReportedHotspotCrimeType;

@Repository
public interface ReportedHotspotCrimeTypeRepository extends JpaRepository<ReportedHotspotCrimeType, Integer> 
{
	
	public ReportedHotspotCrimeType findById(Long parseLong);
	//public List<ReportedHotspotCrimeType> findByDescriptionStartingWith(String description);

	List<ReportedHotspotCrimeType> findByReportedHotspot(ReportedHotspot hotspot);
}
