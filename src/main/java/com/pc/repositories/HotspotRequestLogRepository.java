package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.HotspotRequestLog;

@Repository
public interface HotspotRequestLogRepository extends JpaRepository<HotspotRequestLog, Integer> 
{
	
	public HotspotRequestLog findById(Long parseLong);
	//public List<HotspotRequestLog> findByDescriptionStartingWith(String description);
}
