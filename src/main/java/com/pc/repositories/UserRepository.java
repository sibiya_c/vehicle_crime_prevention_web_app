package com.pc.repositories;

import com.pc.entities.User;
import com.pc.entities.lookup.Gender;
import com.pc.entities.lookup.Title;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("select c from User c where c.email = ?1")
    User getUserByEmail(String email);

    @Query("select c from User c where c.idNumber = ?1")
    User getUserByIdNumber(String rsaId);

    User findByEmail(String email);

    User findByEmailAndIdNumberNot(String email, String idNumber);

    User findByIdAndLoginToken(Long id,String token);

    long countByTitle(Title title);

    long countByGender(Gender gender);

    long count();


}
