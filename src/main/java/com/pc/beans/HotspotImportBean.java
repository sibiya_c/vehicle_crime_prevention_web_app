package com.pc.beans;

import com.pc.annotations.CSVAnnotation;

import java.io.Serializable;

public class HotspotImportBean implements Serializable {
    @CSVAnnotation(name = "crime_type", className = String.class)
    private String crimeType;

    @CSVAnnotation(name = "address_line_1", className = String.class)
    private String address1;

    @CSVAnnotation(name = "address_line_2", className = String.class)
    private String address2;

    @CSVAnnotation(name = "address_line_3", className = String.class)
    private String address3;

    @CSVAnnotation(name = "address_line_4", className = String.class)
    private String address4;

    @CSVAnnotation(name = "address_line_4", className = String.class)
    private String address5;

    public String getCrimeType() {
        return crimeType;
    }

    public void setCrimeType(String crimeType) {
        this.crimeType = crimeType;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getAddress5() {
        return address5;
    }

    public void setAddress5(String address5) {
        this.address5 = address5;
    }

    public String getFormattedAddress(){
        String address=address1;
        if(address2!=null){
            address +=","+address2;
        }

        if(address3!=null){
            address +=","+address3;
        }

        if(address4!=null){
            address +=","+address4;
        }

        if(address5!=null){
            address +=","+address5;
        }

        return address;
    }

    @Override
    public String toString() {
        return "HotspotImportBean{" +
                "crimeType='" + crimeType + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", address3='" + address3 + '\'' +
                ", address4='" + address4 + '\'' +
                ", address5='" + address5 + '\'' +
                '}';
    }
}
