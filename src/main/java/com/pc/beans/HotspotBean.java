package com.pc.beans;

import java.io.Serializable;
import java.util.List;

public class HotspotBean implements Serializable {

    private double latitude;
    private double longitude;
    private List<String> crimeTypeList;
    private List<String> safetyTipsList;
    private String address;
    private String additionalInfo;
    private Long id;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<String> getCrimeTypeList() {
        return crimeTypeList;
    }

    public void setCrimeTypeList(List<String> crimeTypeList) {
        this.crimeTypeList = crimeTypeList;
    }

    public List<String> getSafetyTipsList() {
        return safetyTipsList;
    }

    public void setSafetyTipsList(List<String> safetyTipsList) {
        this.safetyTipsList = safetyTipsList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "HotspotBean{" +
            "latitude=" + latitude +
            ", longitude=" + longitude +
            ", crimeTypeList=" + crimeTypeList +
            ", safetyTipsList=" + safetyTipsList +
            ", address='" + address + '\'' +
            ", additionalInfo='" + additionalInfo + '\'' +
            ", id='" + id + '\'' +
            '}';
    }
}
