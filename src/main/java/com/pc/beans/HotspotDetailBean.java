package com.pc.beans;

import java.util.List;

public class HotspotDetailBean {

    private String radius;
    private boolean success;
    private String errorMessage;
    private List<HotspotBean> hotspotBeanList;

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public List<HotspotBean> getHotspotBeanList() {
        return hotspotBeanList;
    }

    public void setHotspotBeanList(List<HotspotBean> hotspotBeanList) {
        this.hotspotBeanList = hotspotBeanList;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "HotspotDetailBean{" +
            "radius='" + radius + '\'' +
            ", success=" + success +
            ", errorMessage='" + errorMessage + '\'' +
            ", hotspotBeanList=" + hotspotBeanList +
            '}';
    }
}
