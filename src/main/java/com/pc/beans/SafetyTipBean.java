package com.pc.beans;

import java.io.Serializable;
import java.util.List;

public class SafetyTipBean implements Serializable {

    private String title;
    private List<String> tipList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getTipList() {
        return tipList;
    }

    public void setTipList(List<String> tipList) {
        this.tipList = tipList;
    }
}
