package com.pc.entities;

import com.pc.framework.IDataEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "hotspot")
public class Hotspot implements IDataEntity {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", length = 19)
    private Date createDate;

    @Column(name = "last_update_date", length = 19)
    private Date lastUpdateDate;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "active")
    private boolean active;

    @Column(name = "additional_info")
    private String additionalInfo;

    @Column(name = "formatted_address", length = 500)
    private String formattedAddress;

    @Transient
    private List<HotspotCrimeType> hotspotCrimeTypeList;

    @Transient
    private List<HotspotSafetyTip> hotspotSafetyTips;

    @Transient
    private String processError;


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Hotspot other = (Hotspot) obj;
        if (id == null) {
            return other.id == null;
        } else return id.equals(other.id);
    }


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public List<HotspotCrimeType> getHotspotCrimeTypeList() {
        return hotspotCrimeTypeList;
    }

    public void setHotspotCrimeTypeList(List<HotspotCrimeType> hotspotCrimeTypeList) {
        this.hotspotCrimeTypeList = hotspotCrimeTypeList;
    }

    public List<HotspotSafetyTip> getHotspotSafetyTips() {
        return hotspotSafetyTips;
    }

    public void setHotspotSafetyTips(List<HotspotSafetyTip> hotspotSafetyTips) {
        this.hotspotSafetyTips = hotspotSafetyTips;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public String getProcessError() {
        return processError;
    }

    public void setProcessError(String processError) {
        this.processError = processError;
    }

    public String getFormattedSafetyTips() {
        String safetyTips = "";
        if (hotspotSafetyTips != null) {
            for (HotspotSafetyTip hotspotSafetyTip : hotspotSafetyTips) {
                safetyTips += hotspotSafetyTip.getSafetyTip().getDescription() + "|";
            }
        }
        return safetyTips;
    }

    public String getFormattedCrimeType() {
        String crimeType = "";
        if (hotspotCrimeTypeList != null) {
            for (HotspotCrimeType hotspotCrimeType : hotspotCrimeTypeList) {
                crimeType += hotspotCrimeType.getCrimeType().getDescription() + "|";
            }
        }
        return crimeType;
    }
}