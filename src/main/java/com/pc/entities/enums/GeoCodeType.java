package com.pc.entities.enums;

import com.pc.constants.AppConstants;

// TODO: Auto-generated Javadoc

/**
 * The Enum WspTypeEnum.
 */
public enum GeoCodeType {

    AddressToCoordinates("Address To Coordinates") {
        @Override
        public Long getYesNoLookupId() {
            return AppConstants.YES_ID;
        }
    },

    CoordinateToAddress("Coordinate To Address") {
        @Override
        public Long getYesNoLookupId() {
            return AppConstants.NO_ID;
        }
    };

    private String displayName;
    private Long id;

    GeoCodeType(String displayNameX) {
        displayName = displayNameX;
    }


    public static final GeoCodeType getIdPassportEnumByValue(int value) {
        for (GeoCodeType status : GeoCodeType.values()) {
            if (status.ordinal() == value) return status;
        }

        return null;
    }

    public String getFriendlyName() {
        return toString();
    }

    public Long getYesNoLookupId() {
        return id;
    }
}
