package com.pc.entities.enums;

public enum TitleGenderEnum {

    Male("Male"),
    Female("Female"),
    MaleAndFemale("Male and Female");

    private String displayName;

    TitleGenderEnum(String displayNameX) {
        displayName = displayNameX;
    }

    @Override
    public String toString() {
        return displayName;
    }

    public String getFriendlyName() {
        return toString();
    }
}
