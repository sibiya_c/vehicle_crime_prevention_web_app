package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.HotspotRequestLog;
import com.pc.framework.AbstractUI;
import com.pc.service.HotspotRequestLogService;

@Component("hotspotRequestLogUI")
@ViewScoped
public class HotspotRequestLogUI extends AbstractUI{

	@Autowired
	HotspotRequestLogService hotspotRequestLogService;
	private ArrayList<HotspotRequestLog> hotspotRequestLogList;
	private HotspotRequestLog hotspotRequestLog;
	private LazyDataModel<HotspotRequestLog> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		hotspotRequestLog = new HotspotRequestLog();
		loadHotspotRequestLogInfo();
	}

	public void saveHotspotRequestLog()
	{
		try {
			hotspotRequestLogService.saveHotspotRequestLog(hotspotRequestLog);
			displayInfoMssg("Update Successful...!!");
			loadHotspotRequestLogInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteHotspotRequestLog()
	{
		try {
			hotspotRequestLogService.deleteHotspotRequestLog(hotspotRequestLog);
			displayWarningMssg("Update Successful...!!");
			loadHotspotRequestLogInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<HotspotRequestLog> findAllHotspotRequestLog()
	{
		List<HotspotRequestLog> list=new ArrayList<>();
		try {
			list= hotspotRequestLogService.findAllHotspotRequestLog();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<HotspotRequestLog> findAllHotspotRequestLogPageable()
	{
		Pageable p=null;
		try {
			return hotspotRequestLogService.findAllHotspotRequestLog(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<HotspotRequestLog> findAllHotspotRequestLogSort()
	{
		Sort s=null;
		List<HotspotRequestLog> list=new ArrayList<>();
		try {
			list =hotspotRequestLogService.findAllHotspotRequestLog(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		hotspotRequestLog = new HotspotRequestLog();
	}
	
	public void loadHotspotRequestLogInfo()
	{
		 dataModel = new LazyDataModel<HotspotRequestLog>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<HotspotRequestLog> list = new  ArrayList<HotspotRequestLog>();
			   
			   @Override 
			   public List<HotspotRequestLog> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<HotspotRequestLog>) entityDAOFacade.getResultList(HotspotRequestLog.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,HotspotRequestLog.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(HotspotRequestLog obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public HotspotRequestLog getRowData(String rowKey) {
			        for(HotspotRequestLog obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public HotspotRequestLog getHotspotRequestLog() {
		return hotspotRequestLog;
	}

	public void setHotspotRequestLog(HotspotRequestLog hotspotRequestLog) {
		this.hotspotRequestLog = hotspotRequestLog;
	}
	
	public ArrayList<HotspotRequestLog> getHotspotRequestLogList() {
		return hotspotRequestLogList;
	}

	public void setHotspotRequestLogList(ArrayList<HotspotRequestLog> hotspotRequestLogList) {
		this.hotspotRequestLogList =hotspotRequestLogList;
	}
	public LazyDataModel<HotspotRequestLog> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<HotspotRequestLog> dataModel) {
		this.dataModel = dataModel;
	}
	

}
