package com.pc.ui;

import com.pc.beans.HotspotImportBean;
import com.pc.beans.AddressDetails;
import com.pc.client.GeoCodeClient;
import com.pc.entities.Hotspot;
import com.pc.entities.HotspotCrimeType;
import com.pc.entities.HotspotSafetyTip;
import com.pc.entities.enums.GeoCodeType;
import com.pc.entities.lookup.CrimeType;
import com.pc.entities.lookup.SafetyTip;
import com.pc.framework.AbstractUI;
import com.pc.service.HotspotService;
import com.pc.service.lookup.AppConfigService;
import com.pc.service.lookup.CrimeTypeService;
import com.pc.service.lookup.SafetyTipService;
import com.pc.utils.CSVUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@Component
@ManagedBean(name = "hotspotImportUI")
@ViewScoped
public class HotspotImportUI extends AbstractUI {
    private static final Logger logger = LogManager.getLogger(HotspotImportUI.class);
    private ArrayList<Hotspot> hotspotList = new ArrayList<>();
    @Autowired
    private CrimeTypeService crimeTypeService;
    @Autowired
    private SafetyTipService safetyTipService;
    @Autowired
    private AppConfigService appConfigService;
    @Autowired
    private HotspotService hotspotService;

    private CSVUtil csvUtil = new CSVUtil();

    private List<CrimeType> crimeTypeList;

    private List<SafetyTip> safetyTipList;

    private String apiKey = null;

    @PostConstruct
    public void init() {
        try {
            crimeTypeList = crimeTypeService.findAllCrimeType();
            safetyTipList = safetyTipService.findAllSafetyTip();
            apiKey = appConfigService.findByCode("GEO_CODING_API_KEY").getValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uploadHotspots(FileUploadEvent event) {
        try {

            logger.info("Starting file upload");
            List<HotspotImportBean> csvDataList = csvUtil.getObjects(HotspotImportBean.class, event.getFile().getInputstream(), ",");
            for (HotspotImportBean importBean : csvDataList) {
                logger.info("Hotspot Import Bean: {}", importBean);

                hotspotList.add(buildHotspot(importBean));
            }

            logger.info("Finished uploading data. number of entries: " + csvDataList.size());
            displayInfoMssg("Hotspot data uploaded");

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        } finally {
            csvUtil = new CSVUtil();
        }
    }

    public void saveImportedHotspot() {
        try {
            for (Hotspot hotspot : hotspotList) {
                if (hotspot.getProcessError() == null) {
                    List<Hotspot> hotspotList = hotspotService.findByLongitudeAndLatitude(hotspot.getLongitude(), hotspot.getLatitude());
                    if (hotspotList == null || hotspotList.isEmpty()) {
                        List<CrimeType> selectedCrimeTypeList = new ArrayList<>();
                        for (HotspotCrimeType hotspotCrimeType : hotspot.getHotspotCrimeTypeList()) {
                            selectedCrimeTypeList.add(hotspotCrimeType.getCrimeType());
                        }
                        List<SafetyTip> safetyTipList = new ArrayList<>();
                        for (HotspotSafetyTip safetyTip : hotspot.getHotspotSafetyTips()) {
                            safetyTipList.add(safetyTip.getSafetyTip());
                        }
                        hotspotService.saveHotspot(hotspot, selectedCrimeTypeList, safetyTipList);
                    } else {
                        logger.info("hotspot already exist: {}", hotspot.getFormattedAddress());
                    }
                }
            }
            reset();
            displayInfoMssg("Hotspot imported");
            ajaxRedirect("/admin/hotspot.xhtml");
        } catch (Exception e) {
            logger.error("Error: ", e);
        }
    }

    public void reset() {
        hotspotList = new ArrayList<>();
    }

    private Hotspot buildHotspot(HotspotImportBean importBean) {
        List<HotspotCrimeType> hotspotCrimeTypeList = new ArrayList<>();
        List<HotspotSafetyTip> hotspotSafetyTips = new ArrayList<>();
        Hotspot hotspot = new Hotspot();

        for (SafetyTip safetyTip : safetyTipList) {
            if (safetyTip.getApplicableForAllHotspot() != null && safetyTip.getApplicableForAllHotspot()) {
                HotspotSafetyTip hotspotSafetyTip = new HotspotSafetyTip();
                hotspotSafetyTip.setHotspot(hotspot);
                hotspotSafetyTip.setSafetyTip(safetyTip);
                hotspotSafetyTips.add(hotspotSafetyTip);
            }
        }

        for (CrimeType crimeType : crimeTypeList) {
            if (crimeType.getDescription().equalsIgnoreCase(importBean.getCrimeType())) {
                HotspotCrimeType hotspotCrimeType = new HotspotCrimeType();
                hotspotCrimeType.setCrimeType(crimeType);
                hotspotCrimeType.setHotspot(hotspot);
                hotspotCrimeTypeList.add(hotspotCrimeType);
            }
        }

        hotspot.setHotspotCrimeTypeList(hotspotCrimeTypeList);
        hotspot.setHotspotSafetyTips(hotspotSafetyTips);
        hotspot.setFormattedAddress(importBean.getFormattedAddress());
        hotspot.setActive(true);

        try {
            GeoCodeClient geoCodeClient = new GeoCodeClient();
            AddressDetails addressDetails = geoCodeClient.getGoogleGeoCodeResponse(hotspot.getFormattedAddress(), apiKey, GeoCodeType.AddressToCoordinates);
            if (addressDetails.getStatus().equalsIgnoreCase("OK")) {
                hotspot.setLatitude(String.valueOf(addressDetails.getLat()));
                hotspot.setLongitude(String.valueOf(addressDetails.getLng()));
                hotspot.setFormattedAddress(addressDetails.getFormattedAddress());
            } else {
                hotspot.setProcessError("Unable to find address on google map");
            }
        } catch (Exception e) {
            hotspot.setProcessError("Unable to find address on google map: " + e.getMessage());
            logger.error("Error: ", e);
        }

        return hotspot;
    }

    public ArrayList<Hotspot> getHotspotList() {
        return hotspotList;
    }

    public void setHotspotList(ArrayList<Hotspot> hotspotList) {
        this.hotspotList = hotspotList;
    }
}
