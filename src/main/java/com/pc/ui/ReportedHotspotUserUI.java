package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.ReportedHotspotUser;
import com.pc.framework.AbstractUI;
import com.pc.service.ReportedHotspotUserService;

@Component("reportedHotspotUserUI")
@ViewScoped
public class ReportedHotspotUserUI extends AbstractUI{

	@Autowired
	ReportedHotspotUserService reportedHotspotUserService;
	private ArrayList<ReportedHotspotUser> reportedHotspotUserList;
	private ReportedHotspotUser reportedHotspotUser;
	private LazyDataModel<ReportedHotspotUser> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		reportedHotspotUser = new ReportedHotspotUser();
		loadReportedHotspotUserInfo();
	}

	public void saveReportedHotspotUser()
	{
		try {
			reportedHotspotUserService.saveReportedHotspotUser(reportedHotspotUser);
			displayInfoMssg("Update Successful...!!");
			loadReportedHotspotUserInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteReportedHotspotUser()
	{
		try {
			reportedHotspotUserService.deleteReportedHotspotUser(reportedHotspotUser);
			displayWarningMssg("Update Successful...!!");
			loadReportedHotspotUserInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<ReportedHotspotUser> findAllReportedHotspotUser()
	{
		List<ReportedHotspotUser> list=new ArrayList<>();
		try {
			list= reportedHotspotUserService.findAllReportedHotspotUser();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<ReportedHotspotUser> findAllReportedHotspotUserPageable()
	{
		Pageable p=null;
		try {
			return reportedHotspotUserService.findAllReportedHotspotUser(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<ReportedHotspotUser> findAllReportedHotspotUserSort()
	{
		Sort s=null;
		List<ReportedHotspotUser> list=new ArrayList<>();
		try {
			list =reportedHotspotUserService.findAllReportedHotspotUser(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		reportedHotspotUser = new ReportedHotspotUser();
	}
	
	public void loadReportedHotspotUserInfo()
	{
		 dataModel = new LazyDataModel<ReportedHotspotUser>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<ReportedHotspotUser> list = new  ArrayList<ReportedHotspotUser>();
			   
			   @Override 
			   public List<ReportedHotspotUser> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<ReportedHotspotUser>) entityDAOFacade.getResultList(ReportedHotspotUser.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,ReportedHotspotUser.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(ReportedHotspotUser obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public ReportedHotspotUser getRowData(String rowKey) {
			        for(ReportedHotspotUser obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public ReportedHotspotUser getReportedHotspotUser() {
		return reportedHotspotUser;
	}

	public void setReportedHotspotUser(ReportedHotspotUser reportedHotspotUser) {
		this.reportedHotspotUser = reportedHotspotUser;
	}
	
	public ArrayList<ReportedHotspotUser> getReportedHotspotUserList() {
		return reportedHotspotUserList;
	}

	public void setReportedHotspotUserList(ArrayList<ReportedHotspotUser> reportedHotspotUserList) {
		this.reportedHotspotUserList =reportedHotspotUserList;
	}
	public LazyDataModel<ReportedHotspotUser> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<ReportedHotspotUser> dataModel) {
		this.dataModel = dataModel;
	}
	

}
