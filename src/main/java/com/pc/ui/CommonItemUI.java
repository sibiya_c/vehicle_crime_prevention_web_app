package com.pc.ui;

import com.pc.common.AES;
import com.pc.entities.Hotspot;
import com.pc.entities.HotspotCrimeType;
import com.pc.entities.HotspotRequestLog;
import com.pc.entities.MailLog;
import com.pc.entities.ReportedHotspot;
import com.pc.entities.ReportedHotspotCrimeType;
import com.pc.entities.ReportedHotspotUser;
import com.pc.entities.UserBrowserInfo;
import com.pc.entities.UserRole;
import com.pc.entities.enums.TitleGenderEnum;
import com.pc.entities.lookup.CrimeType;
import com.pc.entities.lookup.Gender;
import com.pc.entities.lookup.Role;
import com.pc.entities.lookup.SafetyTip;
import com.pc.entities.lookup.Title;
import com.pc.framework.AbstractUI;
import com.pc.service.HotspotCrimeTypeService;
import com.pc.service.HotspotRequestLogService;
import com.pc.service.HotspotService;
import com.pc.service.MailLogService;
import com.pc.service.ReportedHotspotCrimeTypeService;
import com.pc.service.ReportedHotspotService;
import com.pc.service.ReportedHotspotUserService;
import com.pc.service.UserBrowserInfoService;
import com.pc.service.UserRoleService;
import com.pc.service.lookup.CrimeTypeService;
import com.pc.service.lookup.GenderService;
import com.pc.service.lookup.RoleService;
import com.pc.service.lookup.SafetyTipService;
import com.pc.service.lookup.TitleService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component("commonItemUI")
@ViewScoped
public class CommonItemUI extends AbstractUI {

    @Autowired
    GenderService genderService;

    @Autowired
    TitleService titleService;

    @Autowired
    RoleService roleService;

    @Autowired
    UserBrowserInfoService userBrowserInfoService;

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    MailLogService mailLogService;
    @Autowired
    CrimeTypeService crimeTypeService;
    @Autowired
    HotspotService hotspotService;
    @Autowired
    HotspotCrimeTypeService hotspotCrimeTypeService;
    @Autowired
    ReportedHotspotService reportedHotspotService;
    @Autowired
    ReportedHotspotCrimeTypeService reportedHotspotCrimeTypeService;
    @Autowired
    SafetyTipService safetyTipService;
    @Autowired
    ReportedHotspotUserService reportedHotspotUserService;
    @Autowired
    HotspotRequestLogService hotspotRequestLogService;

    @Autowired
    @PostConstruct
    public void init() {

    }

    /**
     * Gets the select items gender.
     *
     * @return the select items gender
     */
    public List<Gender> getSelectItemsGender() {

        List<Gender> l = null;
        try {

            l = genderService.findAllGender();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    /**
     * Gets the select items title.
     *
     * @return the select items title
     */
    public List<Title> getSelectItemsTitle() {

        List<Title> l = null;
        try {

            l = titleService.findAllTitle();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<Role> getSelectItemsRole() {

        List<Role> l = null;
        try {

            l = roleService.findAllRole();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }


    public List<UserBrowserInfo> autoCompleteUserBrowserInfo(String searchText) {
        List<UserBrowserInfo> list = null;
        try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=userBrowserInfoService.findAllUserBrowserInfo();
			}
			else
			{
				list=userBrowserInfoService.findByDescriptionStartingWith(searchText);
			}*/
            list = userBrowserInfoService.findAllUserBrowserInfo();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<UserBrowserInfo> getSelectItemsUserBrowserInfo() {

        List<UserBrowserInfo> l = null;
        try {

            l = userBrowserInfoService.findAllUserBrowserInfo();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<UserRole> autoCompleteUserRole(String searchText) {
        List<UserRole> list = null;
        try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=userRoleService.findAllUserRole();
			}
			else
			{
				list=userRoleService.findByDescriptionStartingWith(searchText);
			}*/
            list = userRoleService.findAllUserRole();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<UserRole> getSelectItemsUserRole() {

        List<UserRole> l = null;
        try {

            l = userRoleService.findAllUserRole();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<MailLog> autoCompleteMailLog(String searchText) {
        List<MailLog> list = null;
        try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=mailLogService.findAllMailLog();
			}
			else
			{
				list=mailLogService.findByDescriptionStartingWith(searchText);
			}*/
            list = mailLogService.findAllMailLog();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<MailLog> getSelectItemsMailLog() {

        List<MailLog> l = null;
        try {

            l = mailLogService.findAllMailLog();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public String substringText(String value, int maxLength) {
        if (value.length() > maxLength) {
            return value.substring(0, maxLength) + "...";
        } else {
            return value;
        }
    }

    public String formatDate(Date date) {
        if (date != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
            return simpleDateFormat.format(date);
        } else {
            return null;
        }
    }

    public String encrypt(String value) {
        return AES.encrypt(value);
    }

    public List<CrimeType> autoCompleteCrimeType(String searchText) {
        List<CrimeType> list = null;
        try {
            if (searchText == null || searchText.isEmpty()) {
                list = crimeTypeService.findAllCrimeType();
            } else {
                list = crimeTypeService.findByDescriptionStartingWith(searchText);
            }
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<CrimeType> getSelectItemsCrimeType() {

        List<CrimeType> l = null;
        try {

            l = crimeTypeService.findAllCrimeType();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<Hotspot> autoCompleteHotspot(String searchText) {
        List<Hotspot> list = null;
        try {
            list = hotspotService.findAllHotspot();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<Hotspot> getSelectItemsHotspot() {

        List<Hotspot> l = null;
        try {

            l = hotspotService.findAllHotspot();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<HotspotCrimeType> autoCompleteHotspotCrimeType(String searchText) {
        List<HotspotCrimeType> list = null;
        try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=hotspotCrimeTypeService.findAllHotspotCrimeType();
			}
			else
			{
				list=hotspotCrimeTypeService.findByDescriptionStartingWith(searchText);
			}*/
            list = hotspotCrimeTypeService.findAllHotspotCrimeType();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<HotspotCrimeType> getSelectItemsHotspotCrimeType() {

        List<HotspotCrimeType> l = null;
        try {

            l = hotspotCrimeTypeService.findAllHotspotCrimeType();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<ReportedHotspot> autoCompleteReportedHotspot(String searchText) {
        List<ReportedHotspot> list = null;
        try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=reportedHotspotService.findAllReportedHotspot();
			}
			else
			{
				list=reportedHotspotService.findByDescriptionStartingWith(searchText);
			}*/
            list = reportedHotspotService.findAllReportedHotspot();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<ReportedHotspot> getSelectItemsReportedHotspot() {

        List<ReportedHotspot> l = null;
        try {

            l = reportedHotspotService.findAllReportedHotspot();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<ReportedHotspotCrimeType> autoCompleteReportedHotspotCrimeType(String searchText) {
        List<ReportedHotspotCrimeType> list = null;
        try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=reportedHotspotCrimeTypeService.findAllReportedHotspotCrimeType();
			}
			else
			{
				list=reportedHotspotCrimeTypeService.findByDescriptionStartingWith(searchText);
			}*/
            list = reportedHotspotCrimeTypeService.findAllReportedHotspotCrimeType();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<ReportedHotspotCrimeType> getSelectItemsReportedHotspotCrimeType() {

        List<ReportedHotspotCrimeType> l = null;
        try {

            l = reportedHotspotCrimeTypeService.findAllReportedHotspotCrimeType();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<SafetyTip> autoCompleteSafetyTip(String searchText) {
        List<SafetyTip> list = null;
        try {
            if (searchText == null || searchText.isEmpty()) {
                list = safetyTipService.findAllSafetyTip();
            } else {
                list = safetyTipService.findByDescriptionStartingWith(searchText);
            }
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<SafetyTip> getSelectItemsSafetyTip() {

        List<SafetyTip> l = null;
        try {

            l = safetyTipService.findAllSafetyTip();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<ReportedHotspotUser> autoCompleteReportedHotspotUser(String searchText) {
        List<ReportedHotspotUser> list = null;
        try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=reportedHotspotUserService.findAllReportedHotspotUser();
			}
			else
			{
				list=reportedHotspotUserService.findByDescriptionStartingWith(searchText);
			}*/
            list = reportedHotspotUserService.findAllReportedHotspotUser();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<ReportedHotspotUser> getSelectItemsReportedHotspotUser() {

        List<ReportedHotspotUser> l = null;
        try {

            l = reportedHotspotUserService.findAllReportedHotspotUser();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<HotspotRequestLog> autoCompleteHotspotRequestLog(String searchText) {
        List<HotspotRequestLog> list = null;
        try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=hotspotRequestLogService.findAllHotspotRequestLog();
			}
			else
			{
				list=hotspotRequestLogService.findByDescriptionStartingWith(searchText);
			}*/
            list = hotspotRequestLogService.findAllHotspotRequestLog();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public List<HotspotRequestLog> getSelectItemsHotspotRequestLog() {

        List<HotspotRequestLog> l = null;
        try {

            l = hotspotRequestLogService.findAllHotspotRequestLog();

        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
        }
        return l;
    }

    public List<SelectItem> getTitleGenderDD() {
        List<SelectItem> l = new ArrayList<SelectItem>();
        for (TitleGenderEnum val : TitleGenderEnum.values()) {
            l.add(new SelectItem(val, val.getFriendlyName()));
        }
        return l;
    }

    //===============DO NOT REMOVE THIS=================//


}
