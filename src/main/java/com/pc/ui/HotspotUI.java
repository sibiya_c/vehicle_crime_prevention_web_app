package com.pc.ui;

import com.pc.beans.AddressDetails;
import com.pc.client.GeoCodeClient;
import com.pc.dao.EntityDAOFacade;
import com.pc.entities.Hotspot;
import com.pc.entities.HotspotCrimeType;
import com.pc.entities.HotspotSafetyTip;
import com.pc.entities.ReportedHotspot;
import com.pc.entities.ReportedHotspotCrimeType;
import com.pc.entities.enums.GeoCodeType;
import com.pc.entities.lookup.CrimeType;
import com.pc.entities.lookup.SafetyTip;
import com.pc.framework.AbstractUI;
import com.pc.service.HotspotCrimeTypeService;
import com.pc.service.HotspotSafetyTipService;
import com.pc.service.HotspotService;
import com.pc.service.ReportedHotspotCrimeTypeService;
import com.pc.service.ReportedHotspotService;
import com.pc.service.lookup.AppConfigService;
import com.pc.service.lookup.CrimeTypeService;
import com.pc.service.lookup.SafetyTipService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.map.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component("hotspotUI")
@ViewScoped
public class HotspotUI extends AbstractUI {

    private static final Logger logger = LogManager.getLogger(HotspotUI.class);
    @Autowired
    private HotspotService hotspotService;
    private ArrayList<Hotspot> hotspotList;
    private Hotspot hotspot;
    private LazyDataModel<Hotspot> dataModel;
    private LazyDataModel<ReportedHotspot> reportedHotspotDataModel;
    private ReportedHotspot reportedHotspot;
    @Autowired
    private EntityDAOFacade entityDAOFacade;
    @Autowired
    private CrimeTypeService crimeTypeService;
    @Autowired
    private SafetyTipService safetyTipService;
    @Autowired
    private ReportedHotspotCrimeTypeService reportedHotspotCrimeTypeService;
    @Autowired
    private AppConfigService appConfigService;
    @Autowired
    private HotspotCrimeTypeService hotspotCrimeTypeService;
    private List<CrimeType> crimeTypeList;
    private List<CrimeType> selectedCrimeTypeList;
    private DualListModel<SafetyTip> safetyTipDualList;
    private List<SafetyTip> safetyTipList = new ArrayList<>();
    @Autowired
    private HotspotSafetyTipService hotspotSafetyTipService;
    @Autowired
    private ReportedHotspotService reportedHotspotService;

    private String searchAdderss;

    @PostConstruct
    public void init() {

        try {
            hotspot = new Hotspot();
            loadHotspotInfo();
            loadReportedHotspotInfo();
            crimeTypeList = crimeTypeService.findAllCrimeType();

            safetyTipList = safetyTipService.findAllSafetyTip();

            prepareHotspotData();

        } catch (Exception e) {
            displayErrorMssg("Service unavailable");
            e.printStackTrace();
        }
    }

    public void prepareUpdate() {
        try {

            hotspot.setHotspotCrimeTypeList(hotspotCrimeTypeService.findByHotspot(hotspot));
            hotspot.setHotspotSafetyTips(hotspotSafetyTipService.findByHotspot(hotspot));

            prepareHotspotData();

            PrimeFaces.current().scrollTo("mainForm");
        } catch (Exception e) {
            displayErrorMssg("Service unavailable");
            e.printStackTrace();
        }
    }

    public void findGPSCoordinates() {
        try {
            clearAddressDetails();
            if (searchAdderss == null || searchAdderss.isEmpty()) {
                displayErrorMssg("Please enter address");
            } else {
                GeoCodeClient geoCodeClient = new GeoCodeClient();
                AddressDetails addressDetails = geoCodeClient
                    .getGoogleGeoCodeResponse(searchAdderss, appConfigService.findByCode("GEO_CODING_API_KEY").getValue(),
                        GeoCodeType.AddressToCoordinates);
                if (addressDetails.getStatus().equalsIgnoreCase("OK")) {
                    hotspot.setLatitude(String.valueOf(addressDetails.getLat()));
                    hotspot.setLongitude(String.valueOf(addressDetails.getLng()));
                    hotspot.setFormattedAddress(addressDetails.getFormattedAddress());
                } else {
                    displayWarningMssg("Unable to find location");
                }
            }

        } catch (Exception e) {
            displayWarningMssg("Unable to find location");
            e.printStackTrace();
        }
    }

    private void prepareHotspotData() throws Exception {

        safetyTipList = safetyTipService.findAllSafetyTip();
        List<SafetyTip> sourceSafetyTipList = safetyTipList;
        List<SafetyTip> targetSafetyTipList = new ArrayList<>();
        selectedCrimeTypeList = new ArrayList<>();

        if (hotspot.getId() != null) {
            if (hotspot.getHotspotCrimeTypeList() != null) {
                for (HotspotCrimeType hotspotCrimeType : hotspot.getHotspotCrimeTypeList()) {
                    selectedCrimeTypeList.add(hotspotCrimeTypeService.findById(hotspotCrimeType.getId()).getCrimeType());
                }
            }

            if (hotspot.getHotspotSafetyTips() != null) {
                for (HotspotSafetyTip hotspotSafetyTip : hotspot.getHotspotSafetyTips()) {
                    targetSafetyTipList.add(hotspotSafetyTipService.findById(hotspotSafetyTip.getId()).getSafetyTip());
                }
            }
        } else {
            for (SafetyTip safetyTip : safetyTipList) {
                if (safetyTip.getApplicableForAllHotspot()) {
                    targetSafetyTipList.add(safetyTip);
                }
            }
        }

        targetSafetyTipList.forEach(sourceSafetyTipList::remove);

        safetyTipDualList = new DualListModel<>(sourceSafetyTipList, targetSafetyTipList);
        PrimeFaces.current().scrollTo("mainForm");
    }

    public void saveHotspot() {
        try {
            if (selectedCrimeTypeList == null || selectedCrimeTypeList.isEmpty()) {
                throw new Exception("Please select at least one type of the crime");
            }
            if (safetyTipDualList.getTarget().size() < 1) {
                throw new Exception("Please select at least one safety tip");
            }
            if (hotspot.getId() != null) {
                hotspotCrimeTypeService.deleteAll(hotspotCrimeTypeService.findByHotspot(hotspot));
                hotspotSafetyTipService.deleteAll(hotspotSafetyTipService.findByHotspot(hotspot));
            }
            hotspotService.saveHotspot(hotspot, selectedCrimeTypeList, safetyTipDualList.getTarget());
            if (reportedHotspot != null) {
                reportedHotspot.setProcessed("YES");
                reportedHotspotService.saveReportedHotspot(reportedHotspot);
            }
            displayInfoMssg("Hotspot updated");
            reset();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }
    }

    public void onPointSelect(PointSelectEvent event) {
        clearAddressDetails();
        LatLng latlng = event.getLatLng();
        hotspot.setLatitude(String.valueOf(latlng.getLat()));
        hotspot.setLongitude(String.valueOf(latlng.getLng()));

        try {
            GeoCodeClient geoCodeClient = new GeoCodeClient();
            String latLng = hotspot.getLatitude() + "," + hotspot.getLongitude();
            AddressDetails addressDetails = geoCodeClient
                .getGoogleGeoCodeResponse(latLng, appConfigService.findByCode("GEO_CODING_API_KEY").getValue(), GeoCodeType.CoordinateToAddress);
            hotspot.setFormattedAddress(addressDetails.getFormattedAddress());
        } catch (Exception e) {
            logger.error("Error when finding address desc by address: ", e);
        }
        displayInfoMssg("location Selected");
    }

    private void clearAddressDetails() {
        hotspot.setFormattedAddress(null);
        hotspot.setLatitude(null);
        hotspot.setLongitude(null);
    }

    public void deleteHotspot() {
        try {
            hotspotCrimeTypeService.deleteAll(hotspotCrimeTypeService.findByHotspot(hotspot));
            hotspotSafetyTipService.deleteAll(hotspotSafetyTipService.findByHotspot(hotspot));
            hotspotService.deleteHotspot(hotspot);
            displayWarningMssg("Hotspot Successful deleted");
            loadHotspotInfo();
            reset();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }
    }

    public List<Hotspot> findAllHotspot() {
        List<Hotspot> list = new ArrayList<>();
        try {
            list = hotspotService.findAllHotspot();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public Page<Hotspot> findAllHotspotPageable() {
        Pageable p = null;
        try {
            return hotspotService.findAllHotspot(p);
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public List<Hotspot> findAllHotspotSort() {
        Sort s = null;
        List<Hotspot> list = new ArrayList<>();
        try {
            list = hotspotService.findAllHotspot(s);
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public void reset() {
        try {
            hotspot = new Hotspot();
            selectedCrimeTypeList = new ArrayList<>();
            searchAdderss = null;
            reportedHotspot = null;
            prepareHotspotData();
            loadReportedHotspotInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void prepareReportedHotspot() {
        try {
            PrimeFaces.current().scrollTo("mainForm");
            hotspot = new Hotspot();
            selectedCrimeTypeList = new ArrayList<>();
            searchAdderss = null;

            hotspot.setLongitude(reportedHotspot.getLongitude());
            hotspot.setLatitude(reportedHotspot.getLatitude());
            hotspot.setAdditionalInfo(reportedHotspot.getAdditionalInfo());
            hotspot.setFormattedAddress(reportedHotspot.getFormattedAddress());

            List<ReportedHotspotCrimeType> reportedHotspotCrimeTypes = reportedHotspotCrimeTypeService.findByReportedHotspot(reportedHotspot);
            reportedHotspotCrimeTypes.forEach(reportedHotspotCrimeType -> selectedCrimeTypeList.add(reportedHotspotCrimeType.getCrimeType()));

        } catch (Exception e) {
            e.printStackTrace();
            displayErrorMssg(e.getMessage());
        }
    }

    public void loadHotspotInfo() {
        dataModel = new LazyDataModel<Hotspot>() {

            private static final long serialVersionUID = 1L;
            private List<Hotspot> list = new ArrayList<Hotspot>();

            @Override
            public List<Hotspot> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

                try {
                    list = (List<Hotspot>) entityDAOFacade.getResultList(Hotspot.class, first, pageSize, sortField, sortOrder, filters);
                    dataModel.setRowCount(entityDAOFacade.count(filters, Hotspot.class));
                } catch (Exception e) {
                    logger.fatal(e);
                }
                return list;
            }

            @Override
            public Object getRowKey(Hotspot obj) {
                return obj.getId();
            }

            @Override
            public Hotspot getRowData(String rowKey) {
                for (Hotspot obj : list) {
                    if (obj.getId().equals(Long.valueOf(rowKey))) {
                        return obj;
                    }
                }
                return null;
            }

        };

    }

    public void loadReportedHotspotInfo() {
        reportedHotspotDataModel = new LazyDataModel<ReportedHotspot>() {

            private static final long serialVersionUID = 1L;
            private List<ReportedHotspot> list = new ArrayList<ReportedHotspot>();

            @Override
            public List<ReportedHotspot> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

                try {
                    if (filters == null) {
                        filters = new HashMap<>();
                    }
                    filters.put("processed", "NO");
                    list = (List<ReportedHotspot>) entityDAOFacade
                        .getResultList(ReportedHotspot.class, first, pageSize, sortField, sortOrder, filters);
                    reportedHotspotDataModel.setRowCount(entityDAOFacade.count(filters, ReportedHotspot.class));
                } catch (Exception e) {
                    logger.fatal(e);
                }
                return list;
            }

            @Override
            public Object getRowKey(ReportedHotspot obj) {
                return obj.getId();
            }

            @Override
            public ReportedHotspot getRowData(String rowKey) {
                for (ReportedHotspot obj : list) {
                    if (obj.getId().equals(Long.valueOf(rowKey))) {
                        return obj;
                    }
                }
                return null;
            }

        };

    }


    public Hotspot getHotspot() {
        return hotspot;
    }

    public void setHotspot(Hotspot hotspot) {
        this.hotspot = hotspot;
    }

    public ArrayList<Hotspot> getHotspotList() {
        return hotspotList;
    }

    public void setHotspotList(ArrayList<Hotspot> hotspotList) {
        this.hotspotList = hotspotList;
    }

    public LazyDataModel<Hotspot> getDataModel() {
        return dataModel;
    }

    public void setDataModel(LazyDataModel<Hotspot> dataModel) {
        this.dataModel = dataModel;
    }

    public List<CrimeType> getCrimeTypeList() {
        return crimeTypeList;
    }

    public void setCrimeTypeList(List<CrimeType> crimeTypeList) {
        this.crimeTypeList = crimeTypeList;
    }

    public List<CrimeType> getSelectedCrimeTypeList() {
        return selectedCrimeTypeList;
    }

    public void setSelectedCrimeTypeList(List<CrimeType> selectedCrimeTypeList) {
        this.selectedCrimeTypeList = selectedCrimeTypeList;
    }

    public DualListModel<SafetyTip> getSafetyTipDualList() {
        return safetyTipDualList;
    }

    public void setSafetyTipDualList(DualListModel<SafetyTip> safetyTipDualList) {
        this.safetyTipDualList = safetyTipDualList;
    }

    public List<SafetyTip> getSafetyTipList() {
        return safetyTipList;
    }

    public void setSafetyTipList(List<SafetyTip> safetyTipList) {
        this.safetyTipList = safetyTipList;
    }

    public String getSearchAdderss() {
        return searchAdderss;
    }

    public void setSearchAdderss(String searchAdderss) {
        this.searchAdderss = searchAdderss;
    }

    public LazyDataModel<ReportedHotspot> getReportedHotspotDataModel() {
        return reportedHotspotDataModel;
    }

    public void setReportedHotspotDataModel(LazyDataModel<ReportedHotspot> reportedHotspotDataModel) {
        this.reportedHotspotDataModel = reportedHotspotDataModel;
    }

    public ReportedHotspot getReportedHotspot() {
        return reportedHotspot;
    }

    public void setReportedHotspot(ReportedHotspot reportedHotspot) {
        this.reportedHotspot = reportedHotspot;
    }
}
