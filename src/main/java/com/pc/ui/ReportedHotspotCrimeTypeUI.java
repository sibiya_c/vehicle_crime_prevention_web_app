package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.ReportedHotspotCrimeType;
import com.pc.framework.AbstractUI;
import com.pc.service.ReportedHotspotCrimeTypeService;

@Component("reportedHotspotCrimeTypeUI")
@ViewScoped
public class ReportedHotspotCrimeTypeUI extends AbstractUI{

	@Autowired
	ReportedHotspotCrimeTypeService reportedHotspotCrimeTypeService;
	private ArrayList<ReportedHotspotCrimeType> reportedHotspotCrimeTypeList;
	private ReportedHotspotCrimeType reportedHotspotCrimeType;
	private LazyDataModel<ReportedHotspotCrimeType> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		reportedHotspotCrimeType = new ReportedHotspotCrimeType();
		loadReportedHotspotCrimeTypeInfo();
	}

	public void saveReportedHotspotCrimeType()
	{
		try {
			reportedHotspotCrimeTypeService.saveReportedHotspotCrimeType(reportedHotspotCrimeType);
			displayInfoMssg("Update Successful...!!");
			loadReportedHotspotCrimeTypeInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteReportedHotspotCrimeType()
	{
		try {
			reportedHotspotCrimeTypeService.deleteReportedHotspotCrimeType(reportedHotspotCrimeType);
			displayWarningMssg("Update Successful...!!");
			loadReportedHotspotCrimeTypeInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<ReportedHotspotCrimeType> findAllReportedHotspotCrimeType()
	{
		List<ReportedHotspotCrimeType> list=new ArrayList<>();
		try {
			list= reportedHotspotCrimeTypeService.findAllReportedHotspotCrimeType();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<ReportedHotspotCrimeType> findAllReportedHotspotCrimeTypePageable()
	{
		Pageable p=null;
		try {
			return reportedHotspotCrimeTypeService.findAllReportedHotspotCrimeType(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<ReportedHotspotCrimeType> findAllReportedHotspotCrimeTypeSort()
	{
		Sort s=null;
		List<ReportedHotspotCrimeType> list=new ArrayList<>();
		try {
			list =reportedHotspotCrimeTypeService.findAllReportedHotspotCrimeType(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		reportedHotspotCrimeType = new ReportedHotspotCrimeType();
	}
	
	public void loadReportedHotspotCrimeTypeInfo()
	{
		 dataModel = new LazyDataModel<ReportedHotspotCrimeType>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<ReportedHotspotCrimeType> list = new  ArrayList<ReportedHotspotCrimeType>();
			   
			   @Override 
			   public List<ReportedHotspotCrimeType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<ReportedHotspotCrimeType>) entityDAOFacade.getResultList(ReportedHotspotCrimeType.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,ReportedHotspotCrimeType.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(ReportedHotspotCrimeType obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public ReportedHotspotCrimeType getRowData(String rowKey) {
			        for(ReportedHotspotCrimeType obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public ReportedHotspotCrimeType getReportedHotspotCrimeType() {
		return reportedHotspotCrimeType;
	}

	public void setReportedHotspotCrimeType(ReportedHotspotCrimeType reportedHotspotCrimeType) {
		this.reportedHotspotCrimeType = reportedHotspotCrimeType;
	}
	
	public ArrayList<ReportedHotspotCrimeType> getReportedHotspotCrimeTypeList() {
		return reportedHotspotCrimeTypeList;
	}

	public void setReportedHotspotCrimeTypeList(ArrayList<ReportedHotspotCrimeType> reportedHotspotCrimeTypeList) {
		this.reportedHotspotCrimeTypeList =reportedHotspotCrimeTypeList;
	}
	public LazyDataModel<ReportedHotspotCrimeType> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<ReportedHotspotCrimeType> dataModel) {
		this.dataModel = dataModel;
	}
	

}
