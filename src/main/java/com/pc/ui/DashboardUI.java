package com.pc.ui;

import com.pc.entities.User;
import com.pc.framework.AbstractUI;
import com.pc.service.HotspotRequestLogService;
import com.pc.service.HotspotService;
import com.pc.service.ReportedHotspotService;
import com.pc.service.UserService;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component("dashboardUI")
@ViewScoped
public class DashboardUI extends AbstractUI {

    @Autowired
    private UserService userService;
    @Autowired
    private ReportedHotspotService reportedHotspotService;
    @Autowired
    private HotspotService hotspotService;
    @Autowired
    private HotspotRequestLogService hotspotRequestLogService;

    private String authUser = "";
    private User currentUser;

    private int totalUsers;
    private int totalReportedHotspots;
    private int totalHotspots;
    private int totalHotspotsRequest;

    @PostConstruct
    public void init() {

        try {

            totalUsers = (int) userService.countAll();
            totalReportedHotspots = (int) reportedHotspotService.countAll();
            totalHotspots = (int) hotspotService.countAll();
            totalHotspotsRequest = (int) hotspotRequestLogService.countAll();

            prepareCurrentUser();
            authUser = showGreeting();

        } catch (Exception e) {
            e.printStackTrace();
            displayErrorMssg(e.getMessage());
        }
    }

    public String showGreeting() {
        if (currentUser != null) {
            return "Hello " + currentUser.getName() + " " + currentUser.getSurname() + "!, thank you for taking part in keeping South Africans safe from vehicle crime";
        } else {
            return "";
        }
    }

    public void prepareCurrentUser() throws Exception {
        Authentication authentication =
            SecurityContextHolder.getContext().getAuthentication();
        currentUser = userService.findByEmail(authentication.getName());
    }


    public String getAuthUser() {
        return authUser;
    }


    public void setAuthUser(String authUser) {
        this.authUser = authUser;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public int getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(int totalUsers) {
        this.totalUsers = totalUsers;
    }

    public int getTotalReportedHotspots() {
        return totalReportedHotspots;
    }

    public void setTotalReportedHotspots(int totalReportedHotspots) {
        this.totalReportedHotspots = totalReportedHotspots;
    }

    public int getTotalHotspots() {
        return totalHotspots;
    }

    public void setTotalHotspots(int totalHotspots) {
        this.totalHotspots = totalHotspots;
    }

    public int getTotalHotspotsRequest() {
        return totalHotspotsRequest;
    }

    public void setTotalHotspotsRequest(int totalHotspotsRequest) {
        this.totalHotspotsRequest = totalHotspotsRequest;
    }

    public String trimErrorMessage(String mssg) {
        if (mssg != null && mssg.length() > 50) {
            return mssg.substring(0, 50) + "...";
        } else if (mssg == null) {
            return "";
        } else {
            return mssg;
        }
    }

    public boolean hideUserWelcomeMssg(){
       return  hasRole("ADMIN");
    }
}
