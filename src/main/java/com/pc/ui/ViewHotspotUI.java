package com.pc.ui;

import com.pc.entities.Hotspot;
import com.pc.framework.AbstractUI;
import com.pc.service.HotspotService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.Circle;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("viewHotspotUI")
@ViewScoped
public class ViewHotspotUI extends AbstractUI {

    @Autowired
    private HotspotService hotspotService;

    private MapModel simpleModel;

    private Marker marker;

    private MapModel circleModel;

    @PostConstruct
    public void init() {
        try {
            simpleModel = new DefaultMapModel();

            List<Hotspot> hotspotList = hotspotService.findByActive(true);
            hotspotList.forEach(hotspot -> simpleModel.addOverlay(new Marker(
                new LatLng(Double.parseDouble(hotspot.getLatitude()),
                    Double.parseDouble(hotspot.getLongitude())), hotspot.getFormattedAddress())));

            circleModel = new DefaultMapModel();

            hotspotList.forEach(hotspot -> {
                LatLng latLng = new LatLng(Double.parseDouble(hotspot.getLatitude()),
                    Double.parseDouble(hotspot.getLongitude()));
                Circle circle = new Circle(latLng, 1000);
                circle.setStrokeColor("#d93c3c");
                circle.setFillColor("#d93c3c");
                circle.setFillOpacity(0.5);
                circle.setData(hotspot.getFormattedAddress());

                circleModel.addOverlay(circle);

            });

        } catch (Exception e) {
            e.printStackTrace();
            displayErrorMssg("Service unavailable");
        }
    }

    public MapModel getSimpleModel() {
        return simpleModel;
    }

    public void onMarkerSelect(OverlaySelectEvent event) {
        marker = (Marker) event.getOverlay();
        displayInfoMssg("Marker Selected: " + marker.getTitle());

    }

    public Marker getMarker() {
        return marker;
    }

    public MapModel getCircleModel() {
        return circleModel;
    }

    public void onCircleSelect(OverlaySelectEvent event) {
        displayInfoMssg("Circle selected");
    }


}
