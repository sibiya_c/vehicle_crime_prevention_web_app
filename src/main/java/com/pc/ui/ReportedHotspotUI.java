package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.pc.beans.AddressDetails;
import com.pc.client.GeoCodeClient;
import com.pc.entities.enums.GeoCodeType;
import com.pc.entities.lookup.CrimeType;
import com.pc.service.lookup.AppConfigService;
import com.pc.service.lookup.CrimeTypeService;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.model.map.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.util.Map;

import com.pc.dao.EntityDAOFacade;

import com.pc.entities.ReportedHotspot;
import com.pc.framework.AbstractUI;
import com.pc.service.ReportedHotspotService;

@Component("reportedHotspotUI")
@ViewScoped
public class ReportedHotspotUI extends AbstractUI {

    @Autowired
    ReportedHotspotService reportedHotspotService;
    private ArrayList<ReportedHotspot> reportedHotspotList;
    private ReportedHotspot reportedHotspot;
    private LazyDataModel<ReportedHotspot> dataModel;
    @Autowired
    private EntityDAOFacade entityDAOFacade;
    @Autowired
    private CrimeTypeService crimeTypeService;
    @Autowired
    private AppConfigService appConfigService;

    private List<CrimeType> crimeTypeList;
    private List<CrimeType> selectedCrimeTypeList;

    private String searchAdderss;


    @PostConstruct
    public void init() {
        try {
            reportedHotspot = new ReportedHotspot();
            loadReportedHotspotInfo();
            crimeTypeList = crimeTypeService.findAllCrimeType();
        } catch (Exception e) {
            e.printStackTrace();
            displayErrorMssg("Service unavailable");
        }

    }

    public void saveReportedHotspot() {
        try {
            if (reportedHotspot.getLongitude() == null || reportedHotspot.getLatitude() == null) {
                displayWarningMssg("Please select location");
            } else {
                reportedHotspotService.saveReportedHotspot(reportedHotspot, selectedCrimeTypeList);
                displayInfoMssg("Vehicle crime hotspot reported");
                loadReportedHotspotInfo();
                reset();
            }
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }
    }

    public void onPointSelect(PointSelectEvent event) {
        clearAddressDetails();
        LatLng latlng = event.getLatLng();
        reportedHotspot.setLatitude(String.valueOf(latlng.getLat()));
        reportedHotspot.setLongitude(String.valueOf(latlng.getLng()));

        try {
            GeoCodeClient geoCodeClient = new GeoCodeClient();
            String latLng = reportedHotspot.getLatitude() + "," + reportedHotspot.getLongitude();
            AddressDetails addressDetails = geoCodeClient.getGoogleGeoCodeResponse(latLng, appConfigService.findByCode("GEO_CODING_API_KEY").getValue(), GeoCodeType.CoordinateToAddress);
            reportedHotspot.setFormattedAddress(addressDetails.getFormattedAddress());
        } catch (Exception e) {
            logger.error("Error when finding address desc by address: ", e);
        }
        displayInfoMssg("location Selected");
    }


    public void findGPSCoordinates() {
        try {
            clearAddressDetails();
            if (searchAdderss == null || searchAdderss.isEmpty()) {
                displayErrorMssg("Please enter address");
            } else {
                GeoCodeClient geoCodeClient = new GeoCodeClient();
                AddressDetails addressDetails = geoCodeClient.getGoogleGeoCodeResponse(searchAdderss, appConfigService.findByCode("GEO_CODING_API_KEY").getValue(), GeoCodeType.AddressToCoordinates);
                if (addressDetails.getStatus().equalsIgnoreCase("OK")) {
                    reportedHotspot.setLatitude(String.valueOf(addressDetails.getLat()));
                    reportedHotspot.setLongitude(String.valueOf(addressDetails.getLng()));
                    reportedHotspot.setFormattedAddress(addressDetails.getFormattedAddress());
                } else {
                    displayWarningMssg("Unable to find location");
                }
            }

        } catch (Exception e) {
            displayWarningMssg("Unable to find location");
            e.printStackTrace();
        }
    }

    private void clearAddressDetails() {
        reportedHotspot.setFormattedAddress(null);
        reportedHotspot.setLatitude(null);
        reportedHotspot.setLongitude(null);
    }

    public void deleteReportedHotspot() {
        try {
            reportedHotspotService.deleteReportedHotspot(reportedHotspot);
            displayWarningMssg("Update Successful...!!");
            loadReportedHotspotInfo();
            reset();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }
    }

    public List<ReportedHotspot> findAllReportedHotspot() {
        List<ReportedHotspot> list = new ArrayList<>();
        try {
            list = reportedHotspotService.findAllReportedHotspot();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public Page<ReportedHotspot> findAllReportedHotspotPageable() {
        Pageable p = null;
        try {
            return reportedHotspotService.findAllReportedHotspot(p);
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public List<ReportedHotspot> findAllReportedHotspotSort() {
        Sort s = null;
        List<ReportedHotspot> list = new ArrayList<>();
        try {
            list = reportedHotspotService.findAllReportedHotspot(s);
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public void reset() {
        reportedHotspot = new ReportedHotspot();
        selectedCrimeTypeList.clear();
        searchAdderss=null;
    }

    public void loadReportedHotspotInfo() {
        dataModel = new LazyDataModel<ReportedHotspot>() {

            private static final long serialVersionUID = 1L;
            private List<ReportedHotspot> list = new ArrayList<ReportedHotspot>();

            @Override
            public List<ReportedHotspot> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

                try {
                    list = (List<ReportedHotspot>) entityDAOFacade.getResultList(ReportedHotspot.class, first, pageSize, sortField, sortOrder, filters);
                    dataModel.setRowCount(entityDAOFacade.count(filters, ReportedHotspot.class));
                } catch (Exception e) {
                    logger.fatal(e);
                }
                return list;
            }

            @Override
            public Object getRowKey(ReportedHotspot obj) {
                return obj.getId();
            }

            @Override
            public ReportedHotspot getRowData(String rowKey) {
                for (ReportedHotspot obj : list) {
                    if (obj.getId().equals(Long.valueOf(rowKey)))
                        return obj;
                }
                return null;
            }

        };

    }

    public ReportedHotspot getReportedHotspot() {
        return reportedHotspot;
    }

    public void setReportedHotspot(ReportedHotspot reportedHotspot) {
        this.reportedHotspot = reportedHotspot;
    }

    public ArrayList<ReportedHotspot> getReportedHotspotList() {
        return reportedHotspotList;
    }

    public void setReportedHotspotList(ArrayList<ReportedHotspot> reportedHotspotList) {
        this.reportedHotspotList = reportedHotspotList;
    }

    public LazyDataModel<ReportedHotspot> getDataModel() {
        return dataModel;
    }

    public void setDataModel(LazyDataModel<ReportedHotspot> dataModel) {
        this.dataModel = dataModel;
    }

    public List<CrimeType> getCrimeTypeList() {
        return crimeTypeList;
    }

    public void setCrimeTypeList(List<CrimeType> crimeTypeList) {
        this.crimeTypeList = crimeTypeList;
    }

    public List<CrimeType> getSelectedCrimeTypeList() {
        return selectedCrimeTypeList;
    }

    public void setSelectedCrimeTypeList(List<CrimeType> selectedCrimeTypeList) {
        this.selectedCrimeTypeList = selectedCrimeTypeList;
    }

    public String getSearchAdderss() {
        return searchAdderss;
    }

    public void setSearchAdderss(String searchAdderss) {
        this.searchAdderss = searchAdderss;
    }
}
