package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.HotspotCrimeType;
import com.pc.framework.AbstractUI;
import com.pc.service.HotspotCrimeTypeService;

@Component("hotspotCrimeTypeUI")
@ViewScoped
public class HotspotCrimeTypeUI extends AbstractUI{

	@Autowired
	HotspotCrimeTypeService hotspotCrimeTypeService;
	private ArrayList<HotspotCrimeType> hotspotCrimeTypeList;
	private HotspotCrimeType hotspotCrimeType;
	private LazyDataModel<HotspotCrimeType> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		hotspotCrimeType = new HotspotCrimeType();
		loadHotspotCrimeTypeInfo();
	}

	public void saveHotspotCrimeType()
	{
		try {
			hotspotCrimeTypeService.saveHotspotCrimeType(hotspotCrimeType);
			displayInfoMssg("Update Successful...!!");
			loadHotspotCrimeTypeInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteHotspotCrimeType()
	{
		try {
			hotspotCrimeTypeService.deleteHotspotCrimeType(hotspotCrimeType);
			displayWarningMssg("Update Successful...!!");
			loadHotspotCrimeTypeInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<HotspotCrimeType> findAllHotspotCrimeType()
	{
		List<HotspotCrimeType> list=new ArrayList<>();
		try {
			list= hotspotCrimeTypeService.findAllHotspotCrimeType();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<HotspotCrimeType> findAllHotspotCrimeTypePageable()
	{
		Pageable p=null;
		try {
			return hotspotCrimeTypeService.findAllHotspotCrimeType(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<HotspotCrimeType> findAllHotspotCrimeTypeSort()
	{
		Sort s=null;
		List<HotspotCrimeType> list=new ArrayList<>();
		try {
			list =hotspotCrimeTypeService.findAllHotspotCrimeType(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		hotspotCrimeType = new HotspotCrimeType();
	}
	
	public void loadHotspotCrimeTypeInfo()
	{
		 dataModel = new LazyDataModel<HotspotCrimeType>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<HotspotCrimeType> list = new  ArrayList<HotspotCrimeType>();
			   
			   @Override 
			   public List<HotspotCrimeType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<HotspotCrimeType>) entityDAOFacade.getResultList(HotspotCrimeType.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,HotspotCrimeType.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(HotspotCrimeType obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public HotspotCrimeType getRowData(String rowKey) {
			        for(HotspotCrimeType obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public HotspotCrimeType getHotspotCrimeType() {
		return hotspotCrimeType;
	}

	public void setHotspotCrimeType(HotspotCrimeType hotspotCrimeType) {
		this.hotspotCrimeType = hotspotCrimeType;
	}
	
	public ArrayList<HotspotCrimeType> getHotspotCrimeTypeList() {
		return hotspotCrimeTypeList;
	}

	public void setHotspotCrimeTypeList(ArrayList<HotspotCrimeType> hotspotCrimeTypeList) {
		this.hotspotCrimeTypeList =hotspotCrimeTypeList;
	}
	public LazyDataModel<HotspotCrimeType> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<HotspotCrimeType> dataModel) {
		this.dataModel = dataModel;
	}
	

}
