package com.pc.ui.lookup;

import com.pc.beans.SafetyTipBean;
import com.pc.dao.EntityDAOFacade;
import com.pc.entities.lookup.SafetyTip;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.SafetyTipService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component("safetyTipUI")
@ViewScoped
public class SafetyTipUI extends AbstractUI {

    @Autowired
    SafetyTipService safetyTipService;
    @Autowired
    EntityDAOFacade entityDAOFacade;
    private List<SafetyTip> safetyTipList;
    private List<SafetyTipBean> safetyTipBeanList;
    private SafetyTip safetyTip;
    private LazyDataModel<SafetyTip> dataModel;

    @PostConstruct
    public void init() {
        try {
            prepareCurrentUser();
            safetyTip = new SafetyTip();
            if (currentUser != null) {
                loadSafetyTipInfo();
            } else {
                safetyTipList = safetyTipService.findAllSafetyTip();
            }

            prepareSafetyTipsBean();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareSafetyTipsBean() {
        safetyTipBeanList = new ArrayList<>();
        if (safetyTipList != null) {
            safetyTipList.forEach(st -> {
                SafetyTipBean tipBean = safetyTipBeanList.stream().filter(
                    safetyTipBean -> safetyTipBean.getTitle().equalsIgnoreCase(st.getTitle()))
                    .findAny().orElse(null);

                if (tipBean != null) {
                    tipBean.getTipList().add(st.getDescription());
                } else {
                    SafetyTipBean safetyTipBean = new SafetyTipBean();
                    safetyTipBean.setTitle(st.getTitle());
                    List<String> tipList = new ArrayList<>();
                    tipList.add(st.getDescription());
                    safetyTipBean.setTipList(tipList);
                    safetyTipBeanList.add(safetyTipBean);
                }
            });
        }
    }

    public void saveSafetyTip() {
        try {
            safetyTipService.saveSafetyTip(safetyTip);
            displayInfoMssg("Update Successful...!!");
            loadSafetyTipInfo();
            reset();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }
    }

    public void deleteSafetyTip() {
        try {
            safetyTipService.deleteSafetyTip(safetyTip);
            displayWarningMssg("Update Successful...!!");
            loadSafetyTipInfo();
            reset();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }
    }

    public List<SafetyTip> findAllSafetyTip() {
        List<SafetyTip> list = new ArrayList<>();
        try {
            list = safetyTipService.findAllSafetyTip();
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public Page<SafetyTip> findAllSafetyTipPageable() {
        Pageable p = null;
        try {
            return safetyTipService.findAllSafetyTip(p);
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public List<SafetyTip> findAllSafetyTipSort() {
        Sort s = null;
        List<SafetyTip> list = new ArrayList<>();
        try {
            list = safetyTipService.findAllSafetyTip(s);
        } catch (Exception e) {
            displayErrorMssg(e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public void reset() {
        safetyTip = new SafetyTip();
    }

    public void loadSafetyTipInfo() {
        dataModel = new LazyDataModel<SafetyTip>() {

            private static final long serialVersionUID = 1L;
            private List<SafetyTip> list = new ArrayList<SafetyTip>();

            @Override
            public List<SafetyTip> load(int first, int pageSize, String sortField,
                SortOrder sortOrder, Map<String, Object> filters) {

                try {
                    list = (List<SafetyTip>) entityDAOFacade
                        .getResultList(SafetyTip.class, first, pageSize, sortField, sortOrder,
                            filters);
                    dataModel.setRowCount(entityDAOFacade.count(filters, SafetyTip.class));
                } catch (Exception e) {
                    logger.fatal(e);
                }
                return list;
            }

            @Override
            public Object getRowKey(SafetyTip obj) {
                return obj.getId();
            }

            @Override
            public SafetyTip getRowData(String rowKey) {
                for (SafetyTip obj : list) {
                    if (obj.getId().equals(Long.valueOf(rowKey))) {
                        return obj;
                    }
                }
                return null;
            }

        };

    }


    public SafetyTip getSafetyTip() {
        return safetyTip;
    }

    public void setSafetyTip(SafetyTip safetyTip) {
        this.safetyTip = safetyTip;
    }

    public List<SafetyTip> getSafetyTipList() {
        return safetyTipList;
    }

    public void setSafetyTipList(List<SafetyTip> safetyTipList) {
        this.safetyTipList = safetyTipList;
    }

    public LazyDataModel<SafetyTip> getDataModel() {
        return dataModel;
    }

    public void setDataModel(LazyDataModel<SafetyTip> dataModel) {
        this.dataModel = dataModel;
    }

    public List<SafetyTipBean> getSafetyTipBeanList() {
        return safetyTipBeanList;
    }

    public void setSafetyTipBeanList(List<SafetyTipBean> safetyTipBeanList) {
        this.safetyTipBeanList = safetyTipBeanList;
    }
}
