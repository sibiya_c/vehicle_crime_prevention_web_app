package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.CrimeType;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.CrimeTypeService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("crimeTypeUI")
@ViewScoped
public class CrimeTypeUI extends AbstractUI{

	@Autowired
	CrimeTypeService crimeTypeService;
	private ArrayList<CrimeType> crimeTypeList;
	private CrimeType crimeType;
	private LazyDataModel<CrimeType> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		crimeType = new CrimeType();
		loadCrimeTypeInfo();
	}

	public void saveCrimeType()
	{
		try {
			crimeTypeService.saveCrimeType(crimeType);
			displayInfoMssg("Update Successful...!!");
			loadCrimeTypeInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteCrimeType()
	{
		try {
			crimeTypeService.deleteCrimeType(crimeType);
			displayWarningMssg("Update Successful...!!");
			loadCrimeTypeInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<CrimeType> findAllCrimeType()
	{
		List<CrimeType> list=new ArrayList<>();
		try {
			list= crimeTypeService.findAllCrimeType();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<CrimeType> findAllCrimeTypePageable()
	{
		Pageable p=null;
		try {
			return crimeTypeService.findAllCrimeType(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<CrimeType> findAllCrimeTypeSort()
	{
		Sort s=null;
		List<CrimeType> list=new ArrayList<>();
		try {
			list =crimeTypeService.findAllCrimeType(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		crimeType = new CrimeType();
	}
	
	public void loadCrimeTypeInfo()
	{
		 dataModel = new LazyDataModel<CrimeType>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<CrimeType> list = new  ArrayList<CrimeType>();
			   
			   @Override 
			   public List<CrimeType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<CrimeType>) entityDAOFacade.getResultList(CrimeType.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,CrimeType.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(CrimeType obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public CrimeType getRowData(String rowKey) {
			        for(CrimeType obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public CrimeType getCrimeType() {
		return crimeType;
	}

	public void setCrimeType(CrimeType crimeType) {
		this.crimeType = crimeType;
	}
	
	public ArrayList<CrimeType> getCrimeTypeList() {
		return crimeTypeList;
	}

	public void setCrimeTypeList(ArrayList<CrimeType> crimeTypeList) {
		this.crimeTypeList =crimeTypeList;
	}
	
	public LazyDataModel<CrimeType> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<CrimeType> dataModel) {
		this.dataModel = dataModel;
	}

}
