package com.pc.mail;

import com.pc.beans.AttachmentBean;
import com.pc.beans.Mail;
import com.pc.entities.DocByte;
import com.pc.entities.MailLog;
import com.pc.service.DocByteService;
import com.pc.service.MailLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Locale;

@Component
public class MailSender {

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private MailLogService mailLogService;

    @Autowired
    private DocByteService docByteService;
    @Autowired
    private JavaMailSender emailSender;

    public void sendMail(Mail mail_info) {


        try {
            SimpleMailMessage mail = new SimpleMailMessage();

            mail.setFrom(mail_info.getFrom());
            mail.setTo(mail_info.getTo());
            mail.setSubject(mail_info.getSubject());
            mail.setText(mail_info.getContent());
            javaMailSender.send(mail);

            MailLog mailLog = new MailLog(mail_info.getFrom(), getToEmails(mail_info), getCCEmails(mail_info), mail_info.getSubject(), mail_info.getContent(), false, "");
            mailLogService.saveMailLog(mailLog);
            createDocByte(mail_info, mailLog);
        } catch (Exception e) {
            try {
                MailLog mailLog = new MailLog(mail_info.getFrom(), getToEmails(mail_info), getCCEmails(mail_info), mail_info.getSubject(), mail_info.getContent(), true, e.getMessage());
                mailLogService.saveMailLog(mailLog);
                createDocByte(mail_info, mailLog);
                e.printStackTrace();
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }


    }

    public void sendSimpleMessageWithAttachment(Mail mail) {

        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setSubject(mail.getSubject());
            helper.setText(mail.getContent());
            helper.setTo(mail.getTo());
            helper.setFrom(mail.getFrom());

            for (FileSystemResource attachment : mail.getAttachments()) {
                File file = attachment.getFile();
                helper.addAttachment(file.getName(), file);
            }

			/*FileSystemResource file = new FileSystemResource(new File("C:/Users/Patrick/Documents/springbootwebapptemplate/SpringBootWebappTemplate/src/main/webapp/images/b1.jpg"));
			helper.addAttachment("CoolImage.jpg", file);*/
            emailSender.send(message);
            MailLog mailLog = new MailLog(mail.getFrom(), getToEmails(mail), getCCEmails(mail), mail.getSubject(), mail.getContent(), false, "");
            mailLogService.saveMailLog(mailLog);
            createDocByte(mail, mailLog);
        } catch (Exception e) {
            try {
                MailLog mailLog = new MailLog(mail.getFrom(), getToEmails(mail), getCCEmails(mail), mail.getSubject(), mail.getContent(), true, e.getMessage());
                mailLogService.saveMailLog(mailLog);
                createDocByte(mail, mailLog);
                e.printStackTrace();
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
    }


    public void sendHtmlEmil(Mail mail) {

        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setSubject(mail.getSubject());
            helper.setText(generateMailHtml(mail.getContent()), true);
            helper.setTo(mail.getTo());
            helper.setFrom(mail.getFrom());
            helper.setBcc(mail.getCc());
            if (mail.getAttachments() != null && mail.getAttachments().size() > 0) {
                for (FileSystemResource attachment : mail.getAttachments()) {
                    File file = attachment.getFile();
                    helper.addAttachment(file.getName(), file);
                }
            }

            emailSender.send(message);
            MailLog mailLog = new MailLog(mail.getFrom(), getToEmails(mail), getCCEmails(mail), mail.getSubject(), mail.getContent(), false, "");
            mailLogService.saveMailLog(mailLog);
            createDocByte(mail, mailLog);
        } catch (Exception e) {
            try {
                MailLog mailLog = new MailLog(mail.getFrom(), getToEmails(mail), getCCEmails(mail), mail.getSubject(), mail.getContent(), true, e.getMessage());
                mailLogService.saveMailLog(mailLog);
                createDocByte(mail, mailLog);
                e.printStackTrace();
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }

    }


    public void sendWithBeanAttachement(Mail mail) {

        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setSubject(mail.getSubject());
            helper.setText(generateMailHtml(mail.getContent()), true);
            helper.setTo(mail.getTo());
            helper.setFrom(mail.getFrom());
            if (mail.getCc() != null) {
                helper.setBcc(mail.getCc());
            }


            if (mail.getAttachmentBeanList() != null && mail.getAttachmentBeanList().size() > 0) {
                for (AttachmentBean attachment : mail.getAttachmentBeanList()) {

                    helper.addAttachment(attachment.getFileName(), attachment.getInputStreamSource(), attachment.getContentType());
                }
            }

            emailSender.send(message);
            MailLog mailLog = new MailLog(mail.getFrom(), getToEmails(mail), getCCEmails(mail), mail.getSubject(), mail.getContent(), false, "");
            mailLogService.saveMailLog(mailLog);
            createDocByte(mail, mailLog);
        } catch (Exception e) {
            try {
                MailLog mailLog = new MailLog(mail.getFrom(), getToEmails(mail), getCCEmails(mail), mail.getSubject(), mail.getContent(), true, e.getMessage());
                mailLogService.saveMailLog(mailLog);
                createDocByte(mail, mailLog);
                e.printStackTrace();
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
    }

    public void createDocByte(Mail mail, MailLog mailLog) throws Exception {
        if (mail != null && mail.getAttachmentBeanList() != null && mail.getAttachmentBeanList().size() > 0) {
            for (AttachmentBean doc : mail.getAttachmentBeanList()) {

                DocByte docByte = new DocByte();
                byte[] bocBytes = new byte[doc.getInputStreamSource().getInputStream().available()];
                docByte.setByteData(bocBytes);
                docByte.setContentType(doc.getContentType());
                docByte.setFileName(doc.getFileName());
                docByte.setTargetClass(mailLog.getClass().getName());
                docByte.setTargetKey(mailLog.getId());
                docByteService.saveDocByte(docByte);

            }
        }

        if (mail != null && mail.getAttachments() != null && mail.getAttachments().size() > 0) {
            for (FileSystemResource doc : mail.getAttachments()) {

                DocByte docByte = new DocByte();
                byte[] bocBytes = new byte[doc.getInputStream().available()];
                docByte.setByteData(bocBytes);
                docByte.setContentType("");
                docByte.setFileName(doc.getFilename());
                docByte.setTargetClass(mailLog.getClass().getName());
                docByte.setTargetKey(mailLog.getId());
                docByteService.saveDocByte(docByte);

            }
        }

    }

    public String generateMailHtml(String body) {
        final String templateFileName = "content";//Name of the HTML template file without extension

        String output = this.templateEngine.process(templateFileName, new Context(Locale.getDefault(), null));

        output = output.replace("#BODY#", body);
        return output;
    }

    public String getCCEmails(Mail mail) {
        String emails = "";
        if (mail != null && mail.getCc() != null && mail.getCc().length > 0) {
            for (String em : mail.getCc()) {
                emails += em + " ";
            }
        }
        return emails;
    }

    public String getToEmails(Mail mail) {
        String emails = "";
        if (mail != null && mail.getTo() != null && mail.getTo().length > 0) {
            for (String em : mail.getTo()) {
                emails += em + " ";
            }
        }
        return emails;
    }
}
